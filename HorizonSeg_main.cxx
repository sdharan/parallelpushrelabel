// This script is for building an API for exxonmobil project with respect to surface segmentation using graph-cuts
#include "HorizonSeg2.h"
#include "LandMark.h"
#include <string>
using namespace std;

enum GC_technique{Vnet,VCEnet};

int main(int argc, char* argv[]){
    if(argc < 9){
        std::cerr << "Usage: " << std::endl;
        std:: cerr << argv[0] << " InputImage" << " Smoothness" << " Method" <<
                   " LandmarkSwitch" << " WindowSize" << " LayersPerPixel" << " OutputImage" <<
            "  bounding box factor " << " GR_FACTOR " << " SEGMENTS ";
        return EXIT_FAILURE;
    }
    // Default input parameter settings: input_image 100 1 1 11 1 output_image 3

    uint D0 = 1;
    uint D1 = 2;
    uint D2 = 0;

    std::cerr << "segments is" << atoi(argv[10]) << std::endl;
    HorizonSegmenter HS2(string(argv[1]), atoi(argv[6]), D0, D1, D2, atoi(argv[10]), atof(argv[9]));


    //    landmarks for Gradientimage3d_2.mhd
//    int LMarray[14][3] = { {0, 24, 78}, {0, 32, 211}, {5, 27, 89}, {5, 32, 175}, {8,73,940}, {8,65,796}, {8,58,670},
//                           {34,76,709},{34,64,529},{34,34,85},
//                           {45,37,83}, {45,54,321}, {45,53,321}, {45,87, 768}}; // 2d array initialization

//        int LMarray[10][3] = { {48,6,234}, {87, 6, 234}, {109,6, 219}, {168,6,213}, {228,6,210}, {15,17,29}, {66,16,235},
//                               {100,16,230},{135,16,219},{213,16,212}
//                               /*{7,14,221}, {68,14,232}, {150,14,215}, {238,14, 203}*/}; // 2d array initialization

//    int LMarray[13][3] = { {12,6,224}, {12, 18, 224}, {36,6,231}, {65,8,234}, {65,19,235}, {99,3,224}, {99,23,236},
//                                   {133,4,217},{133,22,220},{201,3, 214}, {201, 21, 212}, {235, 4, 208}, {235, 15,207}
//                                   /*{7,14,221}, {68,14,232}, {150,14,215}, {238,14, 203}*/}; // 2d array initialization


    //int LMarray[6][3] = {{56 ,6 ,36}, {59 ,22 ,37}, {185 ,6 ,16}, {201 ,22 ,16}, {    240 ,6 ,6} , {    241 ,22 ,4}};
    //int LMarray[6][3] = { {    7  ,13  ,22}, {    35  ,7  ,27},{ 77  ,7  ,34}, {79  ,0  ,34}, { 86  ,5  ,36}, {    178  ,7  ,15}};

    // int LMarray[10][3] = {{4 ,16 ,153},    {14 ,18 ,150},    {36 ,32 ,155},    {56 ,16 ,154},    {95 ,32 ,160},    {99 ,18 ,157},    {106 ,16 ,155},    {156 ,16 ,146},    {163 ,32 ,149},    {227 ,18 ,145}};

    //int LMarray[5][3] = { {8,11,28}, {8,22,42}, {8,14,32},
//                          {3,22,37},{3,10,22} }; // 2d array initialization

    // Resampled crop liang
//   int LMarray[5][3] = { {6,8,9}, {28,8,13}, {52,8,8},
//                         {20,1,12},{68,1,5} }; // 2d array initialization

    int LMarray[12][3] = {{9, 13, 27},{12, 0, 31},{14, 1, 30},{69, 1, 39},{72, 0, 40},{77, 13, 43},{126, 0, 20},{130, 13, 24},{197, 1, 19},{230, 0, 17},{238, 13, 10},{251, 1, 4}};
    // eg.nrrd

    //int LMarray[98][3] = { {25, 310, 914}, {28, 170, 1005}, {30, 341, 906}, {32, 135, 1023}, {51, 216, 981}, {59, 341, 894}, {60, 280, 920}, {62, 254, 947}, {72, 21, 1078}, {89, 46, 1072}, {97, 310, 891}, {97, 85, 1062}, {107, 341, 857}, {144, 21, 1085}, {159, 254, 960}, {177, 341, 858}, {178, 310, 913}, {193, 280, 942}, {215, 363, 824}, {217, 46, 1087}, {225, 254, 967}, {234, 216, 998}, {261, 85, 1083}, {274, 170, 1034}, {277, 46, 1090}, {278, 21, 1113}, {320, 341, 841}, {322, 135, 1062}, {332, 254, 964}, {336, 216, 1000}, {345, 363, 837}, {362, 310, 873}, {383, 46, 1120}, {385, 363, 843}, {404, 280, 900}, {415, 21, 1156}, {439, 363, 855}, {447, 341, 861}, {462, 85, 1111}, {464, 216, 984}, {467, 21, 1148}, {475, 46, 1146}, {499, 341, 875}, {503, 46, 1112}, {508, 254, 920}, {514, 46, 1120}, {515, 280, 898}, {528, 21, 1181}, {536, 46, 1141}, {583, 216, 963}, {596, 135, 1053}, {600, 310, 899}, {602, 170, 1014}, {607, 85, 1108}, {609, 21, 1189}, {620, 46, 1148}, {640, 341, 899}, {651, 21, 1195}, {659, 254, 934}, {677, 310, 909}, {677, 280, 925}, {679, 216, 973}, {679, 170, 1011}, {685, 135, 1044}, {703, 363, 898}, {703, 135, 1035}, {712, 341, 910}, {727, 21, 1198}, {733, 85, 1135}, {741, 46, 1154}, {744, 280, 941}, {745, 170, 1021}, {749, 341, 914}, {757, 216, 992}, {768, 254, 965}, {782, 135, 1045}, {802, 280, 943}, {803, 310, 935}, {827, 254, 973}, {832, 363, 920}, {832, 46, 1179}, {834, 310, 939}, {834, 135, 1063}, {843, 254, 971}, {846, 21, 1199}, {852, 341, 935}, {856, 216, 1015}, {868, 170, 1051}, {878, 85, 1165}, {922, 21, 1209}, {923, 280, 966}, {929, 310, 962}, {930, 363, 940}, {932, 135, 1099}, {937, 341, 956}, {964, 46, 1202}, {969, 216, 1030}, {983, 85, 1191}};

    uint NoLandmarks = sizeof(LMarray)/sizeof(LMarray[0]); // sizeof(LMarray)/sizeof(*LMarray)
    std::cout << "Number of landmarks: " << NoLandmarks << "\n";
    for(uint m = 0; m < NoLandmarks; m++){
        point p;
        p[0] = LMarray[m][0];
        p[1] = LMarray[m][1];
        p[2] = LMarray[m][2];
        HS2.AddLandmark(p);
    }

    HS2.SetSmoothnessConstraint(atoi(argv[2])); // setting smoothness constraint
    HS2.SetGraphCutMethod(atoi(argv[3])); // setting Graph-cut method
    HS2.SetSwitchToPassThroughLandmark(atoi(argv[4])); // radio switch to pass through landmark
    HS2.SetProfileLength(atoi(argv[5])); // setting window size
    HS2.SetOutputImageName(argv[7]); // setting output image name
    HS2.SetNarrowBandFactor(atoi(argv[8])); // bounding box factor

    //    // calling wrapper
    HS2.Solver();
    HS2.WriteOutputImage();
    //    vec3i SegRegion = HS2.GetRegion();
    //    vec2f SegHorizon = HS2.GetBoundary();
}



