#include "HorizonSeg2.h"

#define DEFAULT_COL_PER_SEGMENT 100
#define DEFAULT_SLICES_PER_SEGMENT 5

HorizonSegmenter::HorizonSegmenter(){
    m_origR = 0;
    m_origC = 0;
    m_origS = 0;
    m_R = 0;
    m_C = 0;
    m_S = 0;
    m_g = 0;
    m_smpl_fctr = 1;
    m_D0 = 0;
    m_D1 = 1;
    m_D2 = 2;
}

HorizonSegmenter::HorizonSegmenter(float ***iparray){
    m_inputarray = extract_iparray(iparray, m_R, m_C, m_S);
    m_g = 0;
    m_smpl_fctr = 1;
    m_D0 = 0;
    m_D1 = 1;
    m_D2 = 2;
}

HorizonSegmenter::HorizonSegmenter(string ipfile, int smpl_fctr, unsigned int& d0, unsigned int& d1, unsigned int& d2, int segments, float gr_factor, bool doParallel) {
    m_smpl_fctr = smpl_fctr;
    m_D0 = d0;
    m_D1 = d1;
    m_D2 = d2;
    this->gr_factor = gr_factor;
    this->num_segments = segments;
    float*** iparray  = extractmatrixfromfile(ipfile, m_R, m_C, m_S, m_smpl_fctr, m_D0, m_D1, m_D2);
    m_inputarray = extract_iparray(iparray, m_R, m_C, m_S);
    for(int i = 0; i < m_S; i++) {
        for(int j = 0; j < m_R; j++) {
            free(iparray[i][j]);
        }
        free(iparray[i]);
    }
    free(iparray);
    m_g = 0;
}

HorizonSegmenter::HorizonSegmenter(ImageType::Pointer image, int smpl_fctr, unsigned int& d0, unsigned int& d1, unsigned int& d2){
    m_smpl_fctr = smpl_fctr;
    m_D0 = d0;
    m_D1 = d1;
    m_D2 = d2;
    m_origC = 0;
    m_origR = 0;
    m_origS = 0;
    float*** iparray  = extractmatrixfromimage(image, m_R, m_C, m_S, m_smpl_fctr, m_D0, m_D1, m_D2);
    m_inputarray = extract_iparray(iparray, m_R, m_C, m_S);
    for(int i = 0; i < m_S; i++) {
        for(int j = 0; j < m_R; j++) {
            free(iparray[i][j]);
        }
        free(iparray[i]);
    }
    free(iparray);

    m_g = 0;
}

void HorizonSegmenter::AddLandmark(point p){
    p[m_D2] = floor(p[m_D2]/(float)m_smpl_fctr);
    m_landmark.push_back(p);
}

void HorizonSegmenter::SetSmoothnessConstraint(int smoothness){
    m_smooth = smoothness;
}

void HorizonSegmenter::SetGraphCutMethod(int gc_method){
    m_gcmethod = gc_method;
}

void HorizonSegmenter::SetSwitchToPassThroughLandmark(int lmswitch){
    m_lmswitch = lmswitch;
}

void HorizonSegmenter::SetProfileLength(int stick_length){
    m_sticklength = stick_length;
}

void HorizonSegmenter::SetOutputImageName(string opimagename){
    m_opimagename = opimagename;
}

void HorizonSegmenter::SetNarrowBandFactor(int narrowband){
    m_narrowband = narrowband;
}

void HorizonSegmenter::Solver(){
    // stage 1
    m_stage1 = 0;
    std::cout << "Solver, m_stage1 = 0" << std::endl;
    this->SortLandmarks();
    this->Memoizelandmarks();
    // initialization of 3D pointers
    float*** modelprofiles;
    float*** idxprofiles;
    //    float*** smallarray;
    float*** costs;
    // memory allocation for 3D pointers
    allocate_memory(modelprofiles, m_S, m_sticklength, m_C);
    allocate_memory(idxprofiles, m_S, m_sticklength, m_C);
    this->ExtractProfiles(modelprofiles, idxprofiles);
    // initialization of 3D pointers
    this->SmallImage(idxprofiles);
    //    this->SmallImage(idxprofiles, smallarray);
    allocate_memory(costs, m_S, m_modiR, m_C);
    this->CostCompute(modelprofiles, costs);
    //    this->CostCompute(modelprofiles, smallarray, costs);
    // memory de-allocation for 3D pointers
    //    destruct_memory(smallarray, m_S, m_modiR, m_C);
    destruct_memory(modelprofiles, m_S, m_sticklength, m_C);
    destruct_memory(idxprofiles, m_S, m_sticklength, m_C);
    this->WeightCompute(costs);
    time_t begin1, end1;
    time(&begin1);
    if(m_gcmethod == 0){
        this->DynamicProg(costs);
        destruct_memory(costs, m_S, m_modiR, m_C);
    }else if(m_gcmethod == 1){
        destruct_memory(costs, m_S, m_modiR, m_C);
        this->BuildGraph();
    }
    destruct_memory(m_Extidxprofiles, m_S, m_modiR, m_C);
    time(&end1);
    std::cout << "Time taken for stage 1: " << difftime(end1,begin1) << " seconds\n";

    // stage 2
    m_stage1 = 1;
    std::cout << "Solver, m_stage1 = 1" << std::endl;
    // memory allocation for 3D pointers
    allocate_memory(modelprofiles, m_S, m_sticklength, m_C);
    allocate_memory(idxprofiles, m_S, m_sticklength, m_C);
    this->ExtractProfiles(modelprofiles, idxprofiles);
    this->SmallImage(idxprofiles);
    //    this->SmallImage(idxprofiles, smallarray);
    allocate_memory(costs, m_S, m_modiR, m_C);
    this->CostCompute(modelprofiles, costs);
    //    this->CostCompute(modelprofiles, smallarray, costs);
    // memory de-allocation for 3D pointers
    //    destruct_memory(smallarray, m_S, m_modiR, m_C);
    destruct_memory(modelprofiles, m_S, m_sticklength, m_C);
    destruct_memory(idxprofiles, m_S, m_sticklength, m_C);
    this->WeightCompute(costs);
    time_t begin2, end2;
    time(&begin2);
    if(m_gcmethod == 0){
        this->DynamicProg(costs);
        destruct_memory(costs, m_S, m_modiR, m_C);
    }else if(m_gcmethod == 1){
        destruct_memory(costs, m_S, m_modiR, m_C);
        this->BuildGraph();
    }
    destruct_memory(m_Extidxprofiles, m_S, m_modiR, m_C);
    time(&end2);
    std::cout << "Time taken for stage 1: " << difftime(end2,begin2) << " seconds\n";
    this->HorizonOutput();
    if(m_g){
        delete m_g;
        m_g = 0;
    }
}

void HorizonSegmenter::Memoizelandmarks(){
    //    counting the number of landmarks per slices
    m_lmspersl.resize(m_S);
    for(int s = 0; s < m_S; s++){
        m_lmspersl[s] = 0;
        unsigned int tmp = 0;
        for(unsigned int l = 0; l < m_landmark.size(); l++){
            if(s == m_landmark[l][m_D0]){
                tmp = tmp+1;
                m_lmspersl[s] = tmp;
            }
        }
    }

    // put these landmarks in a vector
    m_landmarks_per_sl.resize(m_S);
    for(int s = 0; s < m_S; s++){
        m_landmarks_per_sl[s].resize(0);
    }
    for(unsigned int l = 0; l < m_landmark.size(); l++) {
        m_landmarks_per_sl[m_landmark[l][m_D0]].push_back(m_landmark[l]);
    }

    // Acquire non-redundant slices that have landmarks
    for(int s = 0; s < m_S; s++){
        if(m_lmspersl[s] > 0){
            m_lm_slices.push_back(s);
        }
    }
    m_stage1_sl = m_lm_slices.size(); // type casting the slice array size
}

void HorizonSegmenter::SortLandmarks(){
    std::sort(m_landmark.begin(), m_landmark.end(), compareD0D2); // sort using vectors
}

void HorizonSegmenter::ExtractProfiles(float*** &modelprofiles,float*** &idxprofiles){
    std::cout << "Model generation stage\n";
    // Acquiring stick profiles only at landmark points
    vec3f LM_stkprofiles;
    vec3f LM_idxprofiles;
    LM_stkprofiles.resize(m_S);
    LM_idxprofiles.resize(m_S);
    for(int s = 0; s < m_S; s++){
        if(m_lmspersl[s] > 0){
            LM_stkprofiles[s].resize(m_sticklength);
            LM_idxprofiles[s].resize(m_sticklength);
            for(int m = 0; m < (m_sticklength); m++){
                int num_lmspersl = m_lmspersl[s];
                int num_cols = stage_param(m_stage1, num_lmspersl, m_C);
                LM_stkprofiles[s][m].resize(num_cols);
                LM_idxprofiles[s][m].resize(num_cols);
                int half_stklen = floor(m_sticklength/2.0f);
                for(int n = 0; n < num_cols; n++){
                    int Sidx, Cidx;
                    float Ridx;
                    if(m_stage1 == 0){
                        Sidx = s;
                        Ridx = m_landmarks_per_sl[s][n][m_D1];
                        Cidx = m_landmarks_per_sl[s][n][m_D2];
                    }else{
                        Sidx = s;
                        Ridx = m_newlm_rows[s][n];
                        Cidx = n;
                    }
                    float modi_Ridx = Ridx - half_stklen+m;
                    //            std::cout << "m_inputarray size: " << m_inputarray.size() << std::endl;
                    //                    std::cout << "Sidx: " << Sidx << ", modi_Ridx: " << modi_Ridx
                    //                              << ", Ridx: " << Ridx << ", Cidx: " << Cidx
                    //                              << ", half_stklen: " << half_stklen
                    //                              << ", m: " << m << std::endl;
                    float IAP, RAP;
                    // taking care of boundary conditions on rows
                    if(modi_Ridx < 0){
                        IAP = m_inputarray[Sidx][0][Cidx];
                        RAP = 0;
                    }else if(modi_Ridx > (m_R-1)){
                        IAP = m_inputarray[Sidx][m_R-1][Cidx];
                        RAP = (m_R-1);
                    }else{
                        IAP = m_inputarray[Sidx][modi_Ridx][Cidx];
                        RAP = modi_Ridx;
                    }
                    LM_stkprofiles[s][m][n] = IAP;
                    LM_idxprofiles[s][m][n] = RAP;
                }
            }
        }
    }

    // Acquiring stick profiles for all columns
    // model profiles for landmark based slices
    if(m_stage1 == 0){
        for(int d = 0; d < m_S; d++){
            if(m_lmspersl[d] > 0){
                left_columns(LM_stkprofiles, LM_idxprofiles, d, modelprofiles, idxprofiles);
                right_columns(LM_stkprofiles, LM_idxprofiles, d, modelprofiles, idxprofiles);
                inbetween_columns(LM_stkprofiles, LM_idxprofiles, d, modelprofiles, idxprofiles);
            }
        }
    }else{
        for(int d = 0; d < m_S; d++){
            if(m_lmspersl[d] > 0){
                for(int m = 0; m < m_sticklength; m++){
                    for(int n = 0; n < m_C; n++){
                        modelprofiles[d][m][n] = LM_stkprofiles[d][m][n];
                        idxprofiles[d][m][n] = LM_idxprofiles[d][m][n];
                    }
                }
            }
        }
        front_slices(modelprofiles, idxprofiles);    // front most slices
        back_slices(modelprofiles, idxprofiles);    // back most slices
        inbetween_slices(modelprofiles, idxprofiles);    // In between slices
    }
}


void HorizonSegmenter::SmallImage(float*** &idxprofiles){
    std::cout << "Apply narrow-band method\n";
    m_modiR = m_narrowband*m_sticklength;
    int num_sl = stage_param(m_stage1,m_stage1_sl, m_S);
    allocate_memory(m_Extidxprofiles, m_S, m_modiR, m_C);
    for(int s = 0; s < num_sl; s++){
        int curr_sl = s;
        if (m_stage1 == 0) {
            curr_sl = m_lm_slices[s]; // slice to evaluate in stage1 vs stage2
        }
        for(int n = 0; n < m_C; n++){
            int topmost_row = idxprofiles[curr_sl][0][n] - ((floor(m_narrowband/2.f))*m_sticklength); // top row of bounding-box
            for(int m = 0; m < m_modiR; m++){
                int row_idx = topmost_row + m;
                m_Extidxprofiles[curr_sl][m][n] = row_idx;
                bdry_row_set(row_idx,m_R);
            }
        }
    }
}

void HorizonSegmenter::CostCompute(float*** &modelprofiles, float*** &costs){
    std::cout << "Cost compute stage\n";
    int half_stklen = floor(m_sticklength/2.0f);
    float max_cost = -1; // because of normalized cross-correlation
    int num_sl = stage_param(m_stage1,m_stage1_sl, m_S);
    // cost 3d vector initialization
    for(int s = 0; s < num_sl; s++){
        int curr_sl = s;
        if (m_stage1 == 0) {
            curr_sl = m_lm_slices[s]; // slice to evaluate in stage1 vs stage2
        }
        for(int m = 0; m < m_modiR; m++){
            for(int n = 0; n < m_C; n++){
                vec1f model_profile(m_sticklength, 0.0f);
                vec1f test_profile(m_sticklength, 0.0f);
                float corr;
                // boundary condition check
                int new_m = m + m_Extidxprofiles[curr_sl][0][n];
                bdry_row_set(new_m, m_R);
                if((new_m >= half_stklen) && (new_m < (m_R-half_stklen))){
                    for(int p = 0; p < m_sticklength; p++){
                        model_profile[p] = modelprofiles[curr_sl][p][n];
                        test_profile[p] = m_inputarray[curr_sl][(new_m-half_stklen)+p][n];
                    }
                    //                Boundary conditions
                }else if(new_m < half_stklen){
                    for(int p = 0; p < m_sticklength; p++){
                        if(p < (half_stklen - new_m)){
                            model_profile[p] = modelprofiles[curr_sl][half_stklen - new_m][n];
                            test_profile[p] = m_inputarray[curr_sl][0][n];
                        }else{
                            model_profile[p] = modelprofiles[curr_sl][p][n];
                            test_profile[p] = m_inputarray[curr_sl][(new_m-half_stklen)+p][n];
                        }
                    }
                }else if(new_m >= (m_R-half_stklen)){
                    for(int p = 0; p < m_sticklength; p++){
                        if(p < (m_R - new_m + half_stklen)){
                            model_profile[p] = modelprofiles[curr_sl][p][n];
                            test_profile[p] = m_inputarray[curr_sl][(new_m-half_stklen)+p][n];
                        }else{
                            model_profile[p] = modelprofiles[curr_sl][(m_R-new_m+half_stklen)-1][n];
                            test_profile[p] = m_inputarray[curr_sl][m_R-1][n];
                        }
                    }
                }
                corr = dotprod(model_profile,test_profile, m_sticklength);
                costs[curr_sl][m][n] = -corr;
                if(costs[curr_sl][m][n] > max_cost){
                    max_cost = costs[curr_sl][m][n];
                }
            }
        }
    }
    std::cout << "Maxiumum cost: " << max_cost << ", lmswitch: " << m_lmswitch << std::endl;
    // scaling costs in order to be compatible with graph-cut(max-flow) algorithm
    for(int s = 0; s < m_S; s++){
        for(int m = 0; m < m_modiR; m++){
            for(int n = 0; n < m_C; n++){
                costs[s][m][n] = costs[s][m][n]*SCALAR;
            }
        }
    }

    // landmark passing check-point
    for(int s = 0; s < m_stage1_sl; s++){
        int curr_sl = m_lm_slices[s];
        for(int n = 0; n < m_C; n++){
            if(m_lmswitch == 1){
                if(m_stage1 == 0){
                    for(unsigned int nn = 0; nn < m_lmspersl[curr_sl]; nn++){
                        if(n == m_landmarks_per_sl[curr_sl][nn][m_D2]){
                            int new_m_R = m_landmarks_per_sl[curr_sl][nn][m_D1] - m_Extidxprofiles[curr_sl][0][n];
                            for(int m = 0; m < m_modiR; m++){
                                if(m == new_m_R)
                                    costs[curr_sl][m][n] = -5 * max_cost * SCALAR;
                                else
                                    costs[curr_sl][m][n] = 5 * max_cost * SCALAR;
                            }
                        }
                    }
                }else{
                    for(unsigned int nn = 0; nn < m_lm_slices.size(); nn++){
                        if(curr_sl == m_lm_slices[nn]){
                            int new_m_R = m_newlm_rows[curr_sl][n] - m_Extidxprofiles[curr_sl][0][n];
                            for(int m = 0; m < m_modiR; m++){
                                if(m == new_m_R)
                                    costs[curr_sl][m][n] = -5 * max_cost * SCALAR;
                                else
                                    costs[curr_sl][m][n] = 5 * max_cost * SCALAR;
                            }
                        }
                    }
                }
            }
        }
    }
}

void HorizonSegmenter::WeightCompute(float*** &costs){
    std::cout << "Weight compute stage\n";
    int num_sl = stage_param(m_stage1,m_stage1_sl, m_S);
    // Weight 3d vector initialization
    m_weights = vec3f(m_S, vec2f (m_modiR, vec1f (m_C, 0.0f) ) );

    for(int s = 0; s < num_sl; s++){
        int curr_sl = s;
        if(m_stage1 == 0){
            curr_sl = m_lm_slices[s]; // slice to evaluate in stage1 vs stage2
        }
        for(int m = 0; m < m_modiR; m++){
            for(int n = 0; n < m_C; n++){
                if(m < (m_modiR-1)){
                    m_weights[curr_sl][m][n] = costs[curr_sl][m][n] - costs[curr_sl][m+1][n];
                }else{
                    m_weights[curr_sl][m][n] = costs[curr_sl][m][n];
                }
            }
        }
    }
}

void HorizonSegmenter::DynamicProg(float*** &costs){
    std::cout << "Dynamic programming stage\n";
    m_DP = vec3i (m_S, vec2i (m_R, vec1i (m_C,0) ) );

    // no flip for stage 1 & flip columns with slices for stage 2
    vec3f new_costs = flip_matrix(m_stage1, costs, m_S, m_modiR, m_C);
    vec3f new_rowidxmatrix = flip_matrix(m_stage1, m_Extidxprofiles, m_S, m_modiR, m_C);
    int new_s, new_c;
    if(m_stage1 == 0){
        new_s = m_S; new_c = m_C;
    }else{
        new_s = m_C; new_c = m_S;
    }

    int num_sl = stage_param(m_stage1,m_stage1_sl, new_s);

    float*** DP_output;
    allocate_memory(DP_output, new_s, m_R, new_c);
    if (m_stage1 == 0) {
        m_newlm_rows.resize(new_s);
    }

    for(int s = 0; s < num_sl; s++){
        int curr_sl = s; // slice to evaluate in stage1 vs stage2
        if (m_stage1 == 0) {
            curr_sl = m_lm_slices[s];
            m_newlm_rows[curr_sl].resize(new_c);
        }
        vec2i LowCostIdx(m_modiR, vec1i (new_c, 0));
        // costs initialization
        vec1f LowCost, LowPrevCost;
        LowCost.resize(m_modiR);
        LowPrevCost.resize(m_modiR);
        for(int m = 0; m < m_modiR; m++){
            LowCost[m] = new_costs[curr_sl][m][0];
            LowPrevCost[m] = new_costs[curr_sl][m][0];
        }
        // only for column 1
        if((m_stage1 == 0) && (m_landmarks_per_sl[curr_sl][0][m_D2] == 0)){ // check if first column has landmark
            for(int m = 0; m < m_modiR; m++){
                int new_lmrow = m_landmarks_per_sl[curr_sl][0][m_D1] - new_rowidxmatrix[s][0][0];
                if(m == new_lmrow){
                    LowCost[m] = -INFVAL;
                    LowPrevCost[m] = -INFVAL;
                }else{
                    LowCost[m] = INFVAL;
                    LowPrevCost[m] = INFVAL;
                }
            }
            // only for slice 1
        }else if((m_stage1 == 1) && (m_lm_slices[0] == 0)){// check if first slice has landmarks
            int new_lmrow = m_newlm_rows[0][s] - new_rowidxmatrix[s][0][0];
            for(int m = 0; m < m_modiR; m++){
                if(m == new_lmrow){
                    LowCost[m] = -INFVAL;
                    LowPrevCost[m] = -INFVAL;
                }else{
                    LowCost[m] = INFVAL;
                    LowPrevCost[m] = INFVAL;
                }
            }
        }

        for(int n = 1; n < new_c; n++){
            for(int m = 0; m < m_modiR; m++){
                float MinCost = 1E7;
                for(int mm = (m - ARCWINDOW); mm < (m + ARCWINDOW); mm++){
                    if ((mm >= 0) && (mm < m_modiR)){ // boundary condition check
                        float CP = abs(mm-m);
                        float CurrentCost = LowPrevCost[mm] + m_smooth * (pow(CP,BETA)) + new_costs[curr_sl][m][n];
                        if(CurrentCost < MinCost){
                            MinCost = CurrentCost;
                            LowCostIdx[m][n] = mm;
                            LowCost[m] = CurrentCost;
                        }
                    }
                }
            }

            // landmark passing check-point
            if(m_lmswitch == 1){
                if(m_stage1 == 0){
                    for(unsigned int nn = 0; nn < m_lmspersl[curr_sl]; nn++){
                        if(n == m_landmarks_per_sl[curr_sl][nn][m_D2]){
                            for(int m = 0; m < m_modiR; m++){
                                int new_lmrow = m_landmarks_per_sl[curr_sl][nn][m_D1] - new_rowidxmatrix[curr_sl][0][n];
                                int modim_R = new_lmrow;
                                if(m == modim_R)
                                    LowCost[m] = -INFVAL;
                                else
                                    LowCost[m] = INFVAL;
                            }
                        }
                    }
                }else{
                    for(unsigned int nn = 0; nn < m_lm_slices.size(); nn++){
                        if(n == m_lm_slices[nn]){
                            int new_lmrow = m_newlm_rows[n][s] - new_rowidxmatrix[s][0][n];
                            int modim_R = new_lmrow;
                            for(int m = 0; m < m_modiR; m++){
                                if(m == modim_R)
                                    LowCost[m] = -INFVAL;
                                else
                                    LowCost[m] = INFVAL;
                            }
                        }
                    }
                }
            }
            for(int m = 0; m < m_modiR; m++){
                LowPrevCost[m] = LowCost[m];
            }
        }

        // Back-tracing
        vec1i MinCostIdx(new_c,0);
        // Finding minimum cost row-index in the last column
        float MinCost = LowCost[0];
        for(int m = 1; m < m_modiR; m++){
            if(LowCost[m] < MinCost){
                MinCostIdx[new_c-1] = m; // last column
                MinCost = LowCost[m];
            }
        }
        // Extracting minimum cost row indices in all-but-last columns by back-tracing
        for(int n = (new_c-1); n > 0; n--){
            MinCostIdx[n-1] = LowCostIdx[MinCostIdx[n]][n];
        }

        for(int n = 0; n < new_c; n++){
            int SegIdx = new_rowidxmatrix[curr_sl][0][n] + MinCostIdx[n];
            // boundary conditions
            if(SegIdx < 0)
                SegIdx = 0;
            else if(SegIdx > (m_R - 1))
                SegIdx = (m_R - 1);
            //            std::cout << SegIdx << " ";
            if(m_stage1 == 0)
                m_newlm_rows[curr_sl][n] = SegIdx;
            for(int m = SegIdx; m < m_R; m++){
                DP_output[curr_sl][m][n] = 1;
            }
        }
        //        std::cout << "\n";
    }
    vec3f flip_DP; // flip back columns and slices based on the stage
    if(m_stage1 == 0){ // no flipping back
        flip_DP = flip_matrix(m_stage1, DP_output, m_S, m_R, m_C);
    }else{ // flip back
        flip_DP = flip_matrix(m_stage1, DP_output, m_C, m_R, m_S);
    }

    destruct_memory(DP_output, new_s, m_R, new_c);
    for(int s = 0; s < m_S; s++){
        for(int m = 0; m < m_R; m++){
            for(int n = 0; n < m_C; n++){
                m_DP[s][m][n] = flip_DP[s][m][n];
            }
        }
    }
}

void HorizonSegmenter::BuildGraph(){
    int num_sl = stage_param(m_stage1,m_stage1_sl, m_S);
    m_gc = vec3i (m_S, vec2i (m_R, vec1i (m_C,0) ) );
    if(m_g)
    {
        delete m_g;
        m_g = 0;
    }

    if(m_stage1 == 0){
        std::cout << "Graph construction stage1\n";
        m_newlm_rows.resize(m_S);
        for(int s = 0; s < num_sl; s++){
            int curr_sl = m_lm_slices[s]; // slice to evaluate in stage1
            m_newlm_rows[curr_sl].resize(m_C);
            //            Vnet edges
            //            int num_edges = (m_modiR * m_C) // T-links
            //                    + ((m_modiR-1) * m_C) // vertical edges
            //                    + (m_modiR * 2 * m_C); // oblique arcs
            //            VCEnet edges
            int num_edges = (m_modiR * m_C) // T-links
                    + ((m_modiR-1) * m_C) // vertical edges
                    +  ((m_modiR-1) * (2*ARCWINDOW) * (m_C-2)) // oblique edges excluding corner columns
                    + ((m_modiR-1) * ARCWINDOW * 2) // oblique edges on corner columns
                    + m_C; // base edges
            m_g = new GraphType(num_edges*2, m_modiR*m_C, m_modiR, m_C, 1, 4, gr_factor);
            graph_construct_stage0(curr_sl);
            // Finding minimum s-t cut
            std::cout << "Graph partition stage1\n";
            m_g -> maxflow();
            cout<< "Flow = " << m_g->getFlowValue() << "\n";
            graph_partition_stage0(curr_sl);
            //m_g->dump_segmentation("ddd.dump");
        }
    } else {
        std::cout << "Graph construction stage2\n";
        //            Vnet edges
        //            int num_edges = (m_S * m_modiR * m_C) // T-links
        //                    + (m_S * (m_modiR-1) * m_C) // vertical edges
        //                    + (m_modiR * 4 * m_S * m_C); // oblique arcs
        //            VCEnet edges
        int num_edges = (m_S * m_modiR * m_C) // T-links
                + (m_S * (m_modiR-1) * m_C) // vertical edges
                + ((m_modiR-1) * (4*ARCWINDOW) * (m_S * m_C)) // oblique edges (appr.)
                + ((m_C) * (m_S-1))+((m_S) * (m_C-1)); // bottom edges
        m_g = new GraphType(2*num_edges, m_S * m_modiR * m_C, m_modiR, m_C, m_S, num_segments, gr_factor);
        graph_construct_stage1();
        // Finding minimum s-t cut
        std::cout << "Graph partition stage2\n";
        m_g -> maxflow();
        //cout<< "Flow = " << flow << "\n";
        cout << "Flow = " << m_g->getFlowValue() << std::endl;

        graph_partition_stage1();
    }
}

void HorizonSegmenter::HorizonOutput(){
    m_segmentboundary.clear();
    vec3i segmentregion(m_S, vec2i (m_R, vec1i (m_C,0) ) );
    vec2f segmentboundary(m_S, vec1f (m_C, 0.0f) );
    for(int s = 0; s < m_S; s++){
        for(int n = 0; n < m_C; n++){
            for(int m = m_R-1; m >= 0; m--){
                if(((m_gcmethod == 0) && (m_DP[s][m][n] == 1)) || ((m_gcmethod == 1) && (m_gc[s][m][n] == 1))){
                    segmentregion[s][m][n] = 1;
                    segmentboundary[s][n] = m; // replaces current m index with previous index
                }
            }
        }
    }
    m_segmentregion = vec3i (m_origS, vec2i (m_origR, vec1i (m_origC,0) ) );
    m_segmentboundary = vec2f (m_origS, vec1f (m_origC, 0.0f) );
    for(int s = 0; s < m_S; s++){
        for(int n = 0; n < (m_C-1); n++){
            int m0 = segmentboundary[s][n];
            int m1 = segmentboundary[s][n+1];
            int orig_n0 = n * m_smpl_fctr;
            int orig_n1 = (n+1) * m_smpl_fctr;
            for(int nn = orig_n0; nn < orig_n1; nn++){
                float m_mid = LinearInterpolator(orig_n0, orig_n1, m0, m1, nn);
                int orig_m = floor(m_mid+0.5);
                m_segmentboundary[s][nn] = orig_m;
                for(int m = orig_m; m < m_origR; m++){
                    m_segmentregion[s][m][nn] = 1;
                }
            }
        }
    }
    for(int s = 0; s < m_origS; s++){
        for(int n = (m_smpl_fctr*(m_C-1)); n < m_origC; n++){
            int orig_m = m_segmentboundary[s][(m_smpl_fctr*(m_C-1)) -1];
            m_segmentboundary[s][n] = orig_m;
            for(int m = orig_m; m < m_origR; m++){
                m_segmentregion[s][m][n] = 1;
            }
        }
    }
    std::cerr << "Segmented boundary:" << m_segmentboundary.size() * m_segmentboundary[0].size()
              << " (" << m_S << " slices and " << m_C << " columns)\n";
}

// for Seg-3D purpose
vec3i HorizonSegmenter::GetRegion(){
    vec3i seg_region(m_origS, vec2i (m_origC, vec1i (m_origR, 0) ) );;
    for(int s = 0; s < m_origS; s++){
        for(int n = 0; n < m_origC; n++){
            for(int m = 0; m < m_origR; m++){
                seg_region[s][n][m] = m_segmentregion[s][m][n];
            }
        }
    }
    return seg_region;
}

vec2f HorizonSegmenter::GetBoundary(){
    vec2f seg_horizon( m_origS, vec1f (m_origC, 0) );
    for(int s = 0; s < m_origS; s++){
        for(int n = 0; n < m_origC; n++){
            seg_horizon[s][n] = m_segmentboundary[s][n];
            //            std::cout <<  seg_horizon[s][n] << " ";
        }
        //        std::cout << "\n";
    }
    return seg_horizon;
}

void HorizonSegmenter::WriteOutputImage(){
    ImageType::Pointer RefImage = ImageType::New();
    //    int nrowOrig = ((m_R-1)/(float)m_layers)+1;
    //    if(m_D0 == 0)
    //        CreateImage(RefImage, m_C, nrowOrig);
    //    else
    //        CreateImage(RefImage, nrowOrig, m_C);
    //    IteratorType Outputit( RefImage, RefImage -> GetRequestedRegion() ); // Iterator instantiation
    //    Outputit.SetDirection(m_D0); // iterator direction
    //    for(Outputit.GoToBegin(); !Outputit.IsAtEnd(); Outputit.NextLine()){
    //        Outputit.GoToBeginOfLine();
    //        while(!Outputit.IsAtEndOfLine() ){
    //            ImageType::IndexType idx = Outputit.GetIndex();
    //            RefImage -> SetPixel(idx, Outputit.Value() );
    //            Outputit.Set(m_segmentregion[idx[m_D1]][idx[m_D0]]);
    //            ++Outputit;
    //        }
    //    }

    CreateImage(RefImage, m_origS, m_origR, m_origC, m_D0, m_D1, m_D2);
    IteratorType output_it( RefImage, RefImage->GetRequestedRegion() );
    for(output_it.GoToBegin(); !output_it.IsAtEnd(); ++output_it){
        ImageType::IndexType idx = output_it.GetIndex();
        output_it.Set( m_segmentregion[ idx[m_D0] ][ idx[m_D1] ][ idx[m_D2] ] );
    }
    WriterType::Pointer segoutputimage = WriterType::New();
    segoutputimage->SetFileName(m_opimagename);
    segoutputimage->SetInput(RefImage);
    segoutputimage->Update();
}


float*** HorizonSegmenter::extractmatrixfromfile(string inputfile, int& nrow, int& ncol, int& nsli, int& smpl_fctr, unsigned int& m_D0, unsigned int& m_D1, unsigned int& m_D2){
    ReaderType::Pointer reader = ReaderType::New(); // object via smart pointer
    reader -> SetFileName(inputfile);
    reader ->Update();
    ImageType::Pointer ipimage = reader->GetOutput();
    return extractmatrixfromimage(ipimage, nrow, ncol, nsli, smpl_fctr, m_D0, m_D1, m_D2);
}

float*** HorizonSegmenter::extractmatrixfromimage(ImageType::Pointer ipimage, int& nrow, int& ncol, int& nsli, int& smpl_fctr, unsigned int& m_D0, unsigned int& m_D1, unsigned int& m_D2) {
    // Getting Height and Width of the image
    m_origS = ipimage -> GetLargestPossibleRegion().GetSize()[m_D0];
    m_origR = ipimage -> GetLargestPossibleRegion().GetSize()[m_D1];
    m_origC = ipimage -> GetLargestPossibleRegion().GetSize()[m_D2];
    std::cout << "Sub-sampling factor: " << smpl_fctr << "\n";
    std::cout << "Slices: " << m_origS << " Rows: " << m_origR << " Columns: " << m_origC <<  "\n";
    ImageType::Pointer resampled_image = image_resample(ipimage, smpl_fctr, m_D2);
    nsli = resampled_image -> GetLargestPossibleRegion().GetSize()[m_D0];
    nrow = resampled_image -> GetLargestPossibleRegion().GetSize()[m_D1];
    ncol = resampled_image -> GetLargestPossibleRegion().GetSize()[m_D2];
    std::cout << "After sub-sampling\n";
    std::cout << "Slices: " << nsli << " Rows: " << nrow << " Columns: " << ncol <<  "\n";

    // 3D array initialization
    float ***ipimg_array;
    ipimg_array = (float***)malloc(nsli*sizeof(float**));
    for(int i = 0; i < nsli; i++){
        ipimg_array[i] = (float**)malloc(nrow*sizeof(float*));
        for(int j = 0; j < nrow; j++)
            ipimg_array[i][j] = (float*)malloc(ncol*sizeof(float));
    }

    // initializing all values of ipimg_array to zero
    for(int s = 0; s < nsli; s++){
        for(int m = 0; m < nrow; m++){
            for(int n = 0; n < ncol; n++)
                ipimg_array[s][m][n] = 0;
        }
    }

    // resampling the image matrix
    ConstIteratorType input_it( resampled_image, resampled_image -> GetRequestedRegion() ); // Iterator instantiation
    for(input_it.GoToBegin(); !input_it.IsAtEnd(); ++input_it){
        ImageType::IndexType idx = input_it.GetIndex();
        ipimg_array[ idx[m_D0] ][ idx[m_D1] ][ idx[m_D2] ] = input_it.Get();
    }
    return ipimg_array;
}

// Extracting input array
vec3f HorizonSegmenter::extract_iparray(float ***inputarray, int &m_R, int &m_C, int& m_S){
    vec3f iparray;
    for(int s = 0; s < m_S; s++){
        iparray.push_back(vec2f() );
        for(int m = 0; m < m_R; m++){
            iparray[s].push_back(vec1f() );
            for (int n = 0; n < m_C; n++){
                iparray[s][m].push_back(inputarray[s][m][n]);
            }
        }
    }
    return iparray;
}

float HorizonSegmenter::BiLinearInterpolator(point p1, point p2, float p1val, float p2val, int p3){
    int ydiff = p2[0] - p1[0];
    int xdiff = p2[1] - p1[1];
    float l2dist = sqrt(double(xdiff*xdiff) + (ydiff*ydiff));
    float alpha = (abs(p3 - p1[1]))/l2dist;
    return((alpha*p2val)+((1-alpha)*p1val));
}

void HorizonSegmenter::left_columns(vec3f& lm_stkprfls, vec3f& lm_idxprfls, int& p, float*** &modelprofiles, float*** &idxprofiles){
    int left_col = m_landmarks_per_sl[p][0][m_D2];
    for(int m = 0; m < (m_sticklength); m++){
        for(int n = 0; n <= left_col; n++){
            modelprofiles[p][m][n] = lm_stkprfls[p][m][0];
            idxprofiles[p][m][n] = lm_idxprfls[p][m][0];
        }
    }
}

void HorizonSegmenter::right_columns(vec3f& lm_stkprfls, vec3f& lm_idxprfls, int& p, float*** &modelprofiles, float*** &idxprofiles){
    int right_col = m_landmarks_per_sl[p][m_lmspersl[p]-1][m_D2];
    for(int m = 0; m < (m_sticklength); m++){
        for(int n = right_col; n < m_C; n++){
            modelprofiles[p][m][n] = lm_stkprfls[p][m][m_lmspersl[p]-1]; // because index starts from '0'
            idxprofiles[p][m][n] = lm_idxprfls[p][m][m_lmspersl[p]-1];
        }
    }
}

void HorizonSegmenter::inbetween_columns(vec3f& lm_stkprfls, vec3f& lm_idxprfls, int& s, float*** &modelprofiles, float*** &idxprofiles){
    for(int p = 0; p < m_lmspersl[s]-1; p++){
        int LM_pt1 = m_landmarks_per_sl[s][p][m_D2];
        int LM_pt2 = m_landmarks_per_sl[s][p+1][m_D2];
        for(int m = 0; m < (m_sticklength); m++){
            for(int n = LM_pt1; n < LM_pt2; n++){
                if(n == LM_pt1){
                    modelprofiles[s][m][n] = lm_stkprfls[s][m][p];
                    idxprofiles[s][m][n] = lm_idxprfls[s][m][p];
                }else{                        // acquire all model values across columns
                    point p1,p2;
                    p1[0]= lm_idxprfls[s][m][p];
                    PointProcess(p1[0], m_R); // taking care of out-of-bounds on rows of model profiles
                    p1[1] = LM_pt1;

                    p2[0] = lm_idxprfls[s][m][p+1];
                    PointProcess(p2[0], m_R); // taking care of out-of-bounds on rows of model profiles
                    p2[1] = LM_pt2;
                    //                    modelprofiles[s][m][n]=BiLinearInterpolator(p1, p2, lm_stkprfls[s][m][p],lm_stkprfls[s][m][p+1], n);
                    modelprofiles[s][m][n] = LinearInterpolator(p1[1], p2[1], lm_stkprfls[s][m][p],lm_stkprfls[s][m][p+1], n);
                    // acquire all model indices across columns (using line 2-point form)
                    //                    float Ypt = p1[0] + (((float)p2[0] - p1[0])/(p2[1] - p1[1]))*(n-p1[1]);
                    float Ypt = LinearInterpolator(p1[1], p2[1],p1[0],p2[0],n);
                    idxprofiles[s][m][n] = floor(Ypt+0.5);
                }
            }
        }
    }
}

void HorizonSegmenter::front_slices(float*** &modelprofiles, float*** &idxprofiles){
    int FrntSl = m_lm_slices[0];// landmark-based front slice
    for(int s = 0; s < FrntSl; s++){
        for(int m = 0; m < (m_sticklength); m++){
            for(int n = 0; n < m_C; n++){
                modelprofiles[s][m][n] = modelprofiles[FrntSl][m][n];
                idxprofiles[s][m][n] = idxprofiles[FrntSl][m][n];
            }
        }
    }
}

void HorizonSegmenter::back_slices(float*** &modelprofiles, float*** &idxprofiles){
    int BckSl = m_lm_slices[m_lm_slices.size()-1];// landmark-based rear slice
    for(int s = BckSl; s < m_S; s++){
        for(int m = 0; m < (m_sticklength); m++){
            for(int n = 0; n < m_C; n++){
                modelprofiles[s][m][n] = modelprofiles[BckSl][m][n];
                idxprofiles[s][m][n] = idxprofiles[BckSl][m][n];
            }
        }
    }
}

void HorizonSegmenter::inbetween_slices(float*** &modelprofiles, float*** &idxprofiles){
    for(unsigned int s = 0; s < m_lm_slices.size()-1; s++){
        int LM_sl1 = m_lm_slices[s];
        int LM_sl2 = m_lm_slices[s+1];
        for(int p = (LM_sl1+1); p < LM_sl2; p++){
            for(int m = 0; m < m_sticklength; m++){
                for(int n = 0; n < m_C; n++){
                    point p1, p2;
                    p1[0] = idxprofiles[LM_sl1][m][n];
                    p1[1] = LM_sl1;
                    p2[0] = idxprofiles[LM_sl2][m][n];
                    p2[1] = LM_sl2;
                    //                    modelprofiles[p][m][n]=BiLinearInterpolator(p1, p2, modelprofiles[LM_sl1][m][n], modelprofiles[LM_sl2][m][n], p);
                    modelprofiles[p][m][n]=LinearInterpolator(p1[1], p2[1], modelprofiles[LM_sl1][m][n], modelprofiles[LM_sl2][m][n], p);
                    // acquire all model row-indices across slices (using line 2-point form)
                    //                    float Ypt = p1[0] + (((float)p2[0] - p1[0])/(p2[1] - p1[1]))*(p-p1[1]);
                    float Ypt = LinearInterpolator(p1[1], p2[1],p1[0],p2[0],p);
                    idxprofiles[p][m][n] = floor(Ypt+0.5);
                }
            }
        }
    }
}

void HorizonSegmenter::edge_cost(vec1i& delta_ij){
    for(int m = 0; m < ARCWINDOW; m++){
        if(m == 0){
            float deltaij0 = DELTASL(m, m_smooth, BETA);
            delta_ij[m] = deltaij0;
        }else{
            float deltaijxplus1 = DELTALB(m, m_smooth, BETA);
            delta_ij[m] = deltaijxplus1;
        }
    }
}
void HorizonSegmenter::graph_construct_stage0(int& current_sl){
    // adding nodes
    // for(int m = 0; m < m_modiR*m_C; m++)
    //     m_g->add_node();

    // adding edges and costs on them
    for(int m = 0; m < m_modiR; m++){
        for(int n = 0; n < m_C; n++){
            // T-links
            float TlnkCost = m_weights[current_sl][m][n];
            m_g->add_edge(m,n,0,DIR_TERM_FROM,0, TlnkCost);

            // N-links
            // arc from a node to the node below it
            if(m < (m_modiR-1)){
                //m_g->add_edge((m*m_C)+n,((m+1)*m_C)+n,INFVAL,0);
                m_g->add_edge(m,n,0,DIR_DOWN,0, INFVAL);
            }else if(m == (m_modiR-1)){ // base graph
                if(n < (m_C-1)) {
                    //m_g->add_edge((m*m_C)+n,((m*m_C)+(n+1)),INFVAL,INFVAL);
                    m_g->add_edge(m,n, 0,DIR_RIGHT,0, INFVAL);
                    m_g->add_edge(m,n+1,0, DIR_LEFT,0, INFVAL);
                }
            }
        }
    }

// Costs on edges with respect to quadratic penalty
    vec1i Delta_ij(ARCWINDOW,0);
    edge_cost(Delta_ij);

//    VCEnet arcs
    for(int m = 0; m < (m_modiR-1); m++){
        for(int n = 0; n < m_C; n++){
            if(n == 0){
                for(int fij = 0; fij < (m_modiR - m); fij++){
                    if(fij < ARCWINDOW){
                        //m_g->add_edge((m*m_C)+n,((m+fij)*m_C)+(n+1),Delta_ij[fij], 0);
                        m_g->add_edge(m,n,0,DIR_RIGHT,fij,Delta_ij[fij]);
                    }
                }
            }else if(n == (m_C-1)){
                for(int fij = 0; fij < (m_modiR - m); fij++){
                    if(fij < ARCWINDOW){
                        //m_g->add_edge((m*m_C)+n,((m+fij)*m_C)+(n-1),Delta_ij[fij], 0);
                        m_g->add_edge(m,n,0, DIR_LEFT, fij,Delta_ij[fij]);
                    }
                }
            }else{
                for(int fij = 0; fij < (m_modiR - m); fij++){
                    if(fij < ARCWINDOW){
//                        m_g->add_edge((m*m_C)+n,((m+fij)*m_C)+(n+1),Delta_ij[fij], 0);
//                        m_g->add_edge((m*m_C)+n,((m+fij)*m_C)+(n-1),Delta_ij[fij], 0);
                        m_g->add_edge(m,n,0,DIR_RIGHT, fij,Delta_ij[fij]);
                        m_g->add_edge(m,n,0,DIR_LEFT,fij, Delta_ij[fij]);
                    }
                }
            }
        }
    }
}
void HorizonSegmenter::graph_construct_stage1(){

    // adding edges and costs on them
    for(int s = 0; s < m_S; s++){
        for(int m = 0; m < m_modiR; m++){
            for(int n = 0; n < m_C; n++){
                // T-links
                float TlnkCost = m_weights[s][m][n];
                m_g->add_edge(m,n,s, DIR_TERM_FROM, 0,TlnkCost);
                // arc from a node to the node below it
                if(m < (m_modiR-1)){
                    m_g->add_edge(m,n,s,DIR_DOWN, 0, INFVAL);
                }else if(m == (m_modiR-1)){ // base graph
                    if(s < (m_S-1)) {
                        m_g->add_edge(m,n,s,DIR_FRONT,0, INFVAL);
                        m_g->add_edge(m,n,s+1, DIR_BACK,0, INFVAL);
                    }
                    if(n < (m_C-1)) {
                        m_g->add_edge(m,n,s,DIR_RIGHT, 0, INFVAL);
                        m_g->add_edge(m,n+1,s, DIR_LEFT,0, INFVAL);
                    }
                }
            }
        }
    }

    // Costs on edges with respect to quadratic penalty
    vec1i Delta_ij(ARCWINDOW,0);
    edge_cost(Delta_ij);

//    VCEnet arcs
    for(int s = 0; s < m_S; s++){
        for(int m = 0; m < (m_modiR-1); m++){
            for(int n = 0; n < m_C; n++){
                if((n == 0) && (s < (m_S-1))){
                    for(int fij = 0; fij < (m_modiR - m); fij++){
                        if(fij < ARCWINDOW){
                            m_g -> add_edge(m,n,s,DIR_FRONT, fij,Delta_ij[fij]);
                            if (s+1 < m_S) {
                                m_g -> add_edge(m,n,s+1,DIR_BACK,fij, Delta_ij[fij]);
                            }
                            m_g->add_edge(m,n,s,DIR_RIGHT,fij, Delta_ij[fij]);
                        }
                    }
                }else if((n == (m_C-1)) && (s < (m_S-1))){
                    for(int fij = 0; fij < (m_modiR - m); fij++){
                        if(fij < ARCWINDOW){
                          if (s+1 < m_S) {
                              m_g->add_edge(m,n,s,DIR_FRONT,fij, Delta_ij[fij]);
                          }
                          m_g->add_edge(m,n,s+1, DIR_BACK,fij, Delta_ij[fij]);
                          m_g->add_edge(m,n,s, DIR_LEFT,fij,Delta_ij[fij]);
                        }
                    }
                }else if((s == 0) && (n < (m_C-1))){
                    for(int fij = 0; fij < (m_modiR - m); fij++){
                        if(fij < ARCWINDOW){
                            m_g->add_edge(m,n,s, DIR_RIGHT,fij, Delta_ij[fij]);
                            m_g->add_edge(m,n+1, s, DIR_LEFT,fij, Delta_ij[fij]);
                            if (s+1 < m_S) {
                                m_g->add_edge(m,n,s, DIR_FRONT,fij, Delta_ij[fij]);
                            }
                        }
                    }
                }else if((s == (m_S-1)) && (n < (m_C-1))){
                    for(int fij = 0; fij < (m_modiR - m); fij++){
                        if(fij < ARCWINDOW){
                            m_g->add_edge(m,n,s, DIR_RIGHT, fij, Delta_ij[fij]);
                            m_g->add_edge(m,n+1,s, DIR_LEFT,fij, Delta_ij[fij]);
                            if (s-1 >= 0) {
                                m_g->add_edge(m,n,s, DIR_BACK, fij, Delta_ij[fij]);
                            }
                        }
                    }
                }else if((s > 0) && (s < (m_S-1)) && (n > 0) && (n < (m_C-1))){
                    for(int fij = 0; fij < (m_modiR - m); fij++){
                        if(fij < ARCWINDOW){
                            m_g->add_edge(m,n,s, DIR_RIGHT, fij, Delta_ij[fij]);
                            m_g->add_edge(m,n,s, DIR_LEFT, fij, Delta_ij[fij]);
                            if (s+1 < m_S) {
                                m_g->add_edge(m,n,s, DIR_FRONT, fij, Delta_ij[fij]);
                            }
                            m_g->add_edge(m,n,s, DIR_BACK, fij, Delta_ij[fij]);
                        }
                    }
                }
            }
        }
    }
}


void HorizonSegmenter::graph_partition_stage0(int &current_sl){
    for(int n = 0; n < m_C;n++){
        for(int m = 0; m < m_modiR; m++){

            if(((m > 0) && ((m_g -> what_segment(m-1,n,0) == 0) &&
                            (m_g -> what_segment(m,n,0) == 1) ) ) ||
                    ((m == 0) && (m_g -> what_segment(m,n,0) == 1) ) ){
                int real_m = m_Extidxprofiles[current_sl][0][n] + m;
                // boundary conditions
                if(real_m < 0)
                    real_m = 0;
                else if(real_m > (m_R - 1))
                    real_m = (m_R - 1);
                for(int mm = real_m; mm < m_R; mm++)
                    m_gc[current_sl][mm][n] = 1;

                m_newlm_rows[current_sl][n] = real_m;
            }
        }
    }
}

void HorizonSegmenter::graph_partition_stage1(){
    for(int s = 0; s < m_S; s++){
        for(int n = 0; n < m_C;n++){
            for(int m = 0; m < m_modiR; m++){

                if(((m > 0) && ((m_g -> what_segment(m-1,n,s) == 0) &&
                                (m_g -> what_segment(m,n,s)  == 1))) ||
                        ((m == 0) && (m_g -> what_segment(m,n,s) == 1) ) ){
                    int real_m = m_Extidxprofiles[s][0][n] + m + 14;

                    // boundary conditions
                    if(real_m < 0)
                        real_m = 0;
                    else if(real_m > (m_R - 1))
                        real_m = (m_R - 1);
                    for(int mm = real_m; mm < m_R; mm++)
                        m_gc[s][mm][n] = 1;
                }
            }
        }
    }

}

// function to compare two points
bool compareD0D2(point a, point b){
    return((a[0] < b[0]) || ((a[0] == b[0]) && (a[2] < b[2])));
}

// function to resample image
ImageType::Pointer image_resample(ImageType::Pointer ip_image, int& smplfctr, unsigned int& d2){
    //ImageType::Pointer image_resample(ImageType::Pointer ip_image, int& S, int& R, int& C, int& smplfctr,
    //                                  unsigned int& d0, unsigned int& d1, unsigned int& d2){
    typedef itk::RecursiveGaussianImageFilter< ImageType, ImageType > GaussianFilterType;
    typedef itk::ShrinkImageFilter< ImageType, ImageType > ShrinkImageFilterType;

    GaussianFilterType::Pointer smoother = GaussianFilterType::New();
    smoother->SetInput(ip_image);
    ImageType::SpacingType inSpacing = ip_image->GetSpacing();
    double sigma = inSpacing[d2] * smplfctr;
    smoother->SetSigma(sigma/6);
    smoother->SetDirection(d2);
    smoother->SetNormalizeAcrossScale(false);

    ShrinkImageFilterType::Pointer shrinker = ShrinkImageFilterType::New();
    shrinker->SetInput( smoother->GetOutput() );
    shrinker->SetShrinkFactor( d2, smplfctr );
    shrinker->Update();

    ImageType::Pointer resampledimage = shrinker->GetOutput();
    //    WriterType::Pointer smoothedimage = WriterType::New();
    //    smoothedimage->SetFileName("smoothed.nhdr");
    //    smoothedimage->SetInput(smoother->GetOutput());
    //    smoothedimage->Update();
    //    WriterType::Pointer resampledwriter = WriterType::New();
    //    resampledwriter->SetFileName("resampled.nhdr");
    //    resampledwriter->SetInput(resampledimage);
    //    resampledwriter->Update();

    return resampledimage;
}

void bdry_row_set(int &curr_row, int &row_limit){
    if(curr_row < 0) // above image row-limit
        curr_row = 0;
    else if(curr_row >= row_limit) // below row-limit
        curr_row = row_limit - 1;
}

// function to take care of out-of-bounds
void PointProcess(int &pt, int &RowMax){
    if(pt < 0)
        pt = 0;
    else if(pt > (RowMax-1))
        pt = RowMax-1;
}

// Linear interpolation (for resampling)
float LinearInterpolator(int& x0, int& x1, int& y0, int& y1, int& x){
    float y;
    float x1minusx0 = (x1-x0)+EPS;
    y = y0 + (y1 - y0)*((x - x0)/x1minusx0);
    return y;
}

float LinearInterpolator(int& x0, int& x1, float& y0, float& y1, int& x){
    float y;
    float x1minusx0 = (x1-x0)+EPS;
    y = y0 + (y1 - y0)*((x - x0)/x1minusx0);
    return y;
}

// function to allocate memory for 3D pointer
void allocate_memory(float*** &vec3d, int &slc, int &row, int &col){
    vec3d = new float**[slc];
    for(int m = 0; m < slc; m++){
        vec3d[m] = new float*[row];
        for(int n = 0; n < row; n++){
            vec3d[m][n] = new float[col];
            memset(reinterpret_cast<void*>(vec3d[m][n]), 0, sizeof(float)*col);
        }
    }
}

// function to destruct memory for 3D pointer
void destruct_memory(float*** &vec3d, int &slc, int &row, int &col){
    for(int m = 0; m < slc; m++){
        for(int n = 0; n < row; n++){
            // delete[] vec3d[m][n];
        }
        // delete[] vec3d[m];
    }
    // delete[] vec3d;
}

// function to select stage parameters
int stage_param(bool stage, int& stage1_param, int& stage2_param){
    int select_param;
    if(stage == 0)
        select_param = stage1_param;
    else
        select_param = stage2_param;

    return select_param;
}

vec1f gauss1d(int& length){
    float SOG = 0;
    vec1f gauss( length, 0.0f );
    int half_len = floor( length/2.0f );
    float SGM = half_len/4.0f;
    for(int x = -half_len; x < half_len; x++){
        float exponent = (1/(2 * SGM * SGM)) * x;
        gauss[x+half_len] = exp(-exponent);
        SOG = SOG + gauss[x+half_len];
    }

    vec1f Ngauss( length, 0.0f );
    for(int x = 0; x < length; x++){
        Ngauss[x] = gauss[x]/SOG;
    }
    return Ngauss;
}

void scale_profile(vec1f& img_prfl, vec1f& gauss_prfl, int& length){
    for(int m = 0; m < length; m++){
        img_prfl[m] = img_prfl[m] * gauss_prfl[m];
    }
}

float dotprod(vector<float>& vec1, vector<float>& vec2, int &length){
    // correlation between two vectors
    float Ncorr;
    // calculating the mean
    float vec1sum = 0;
    float vec2sum = 0;
    for (int m = 0; m < length; m++){
        vec1sum = vec1sum + vec1[m];
        vec2sum = vec2sum + vec2[m];
    }
    float vec1mean = vec1sum/length;
    float vec2mean = vec2sum/length;

    // calculating L2 norm
    float SS_vec1 = 0;
    float SS_vec2 = 0;
    float varprod = 0;
    for (int m = 0; m < length; m++){
        float v1_vrnc = vec1[m] - vec1mean;
        float v2_vrnc = vec2[m] - vec2mean;
        SS_vec1 = SS_vec1 + (v1_vrnc * v1_vrnc);
        SS_vec2 = SS_vec2 + (v2_vrnc * v2_vrnc);
        varprod = varprod + v1_vrnc* v2_vrnc;
    }
    float vec1l2norm = sqrt(SS_vec1);
    if (vec1l2norm == 0)
        vec1l2norm = 1E5;
    float vec2l2norm = sqrt(SS_vec2);
    if (vec2l2norm == 0)
        vec2l2norm = 1E5;

    // Normalized cross-correlation
    Ncorr = varprod/(vec1l2norm*vec2l2norm);
    return Ncorr;
}

vec3f flip_matrix(bool stage, float*** &vec3D, int& d0size, int& d1size, int& d2size){
    vec3f new_vec3Dmatrix;
    if(stage == 0){ // if stage0, no flipping
        new_vec3Dmatrix = vec3f(d0size, vec2f (d1size, vec1f (d2size,0.0f) ) );
        for(int i = 0; i < d0size; i++){
            for(int j = 0; j < d1size; j++){
                for(int k = 0; k < d2size; k++){
                    new_vec3Dmatrix[i][j][k] = vec3D[i][j][k];
                }
            }
        }
    }else{ // if stage1 , flip columns with slices
        new_vec3Dmatrix = vec3f(d2size, vec2f (d1size, vec1f (d0size,0.0f) ) );
        for(int i = 0; i < d2size; i++){
            for(int j = 0; j < d1size; j++){
                for(int k = 0; k < d0size; k++){
                    new_vec3Dmatrix[i][j][k] = vec3D[k][j][i];
                }
            }
        }
    }
    return new_vec3Dmatrix;
}

float DELTASL(unsigned int f, int alpha, int beta){ // SL = same level
    float fa = alpha * (pow((float)(f+1),beta));
    float sl = alpha * (pow((float)f,beta));
    return((fa - sl)); // cost penalty edge function
}

float DELTALB(unsigned int f, int alpha, int beta){ // LB == level below
    float fa = alpha * (pow((float)(f+1),beta));
    float sl = alpha * (pow((float)f,beta));
    float fb = alpha * (pow((float)(f-1),beta));
    return((fa-sl) - (sl-fb)); // cost penalty edge function
}

void CreateImage(ImageType::Pointer image, int& NumSlices, int& NumRows, int& NumCols,
                 unsigned int& d0, unsigned int& d1, unsigned int& d2){
    ImageType::RegionType region;
    ImageType::IndexType start;
    start[d0] = 0;
    start[d1] = 0;
    start[d2] = 0;
    ImageType::SizeType size;
    size[d0] = NumSlices;
    size[d1] = NumRows;
    size[d2] = NumCols;

    region.SetIndex(start);
    region.SetSize(size);
    image -> SetRegions(region);
    image -> Allocate();
    image -> FillBuffer( 0 );
}
