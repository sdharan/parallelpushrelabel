#ifndef DS_H
#define DS_H

#include <iostream>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/atomic.hpp>
#include <assert.h>
#include <queue>
#include <fstream>
#include <time.h>
//include "concurrentqueue.h"
#include "defs.h"
#include <boost/atomic.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/thread/shared_lock_guard.hpp>
#include <boost/thread/shared_mutex.hpp>

using namespace std;

enum status {SUCCESS, WAIT, END};
enum ranktype {WHITE, GREY, BLACK};


// Spinlocks may actually be better than mutex for our implementation.
// TODO analyze timings
// http://stackoverflow.com/questions/19742993/implementing-a-spinlock-in-boost-example-needed
class SpinLock
{
    boost::atomic_flag flag; // it differs from std::atomic_flag a bit -
                             // does not require ATOMIC_FLAG_INIT
public:
    void lock()
    {
        while( flag.test_and_set(boost::memory_order_acquire) )
            ;
    }
    bool try_lock()
    {
        return !flag.test_and_set(boost::memory_order_acquire);
    }
    void unlock()
    {
        flag.clear(boost::memory_order_release);
    }
};

struct SizingOracle {
    size_t plane_size;
    int rows,columns,slices;
    SizingOracle() {}
    SizingOracle(int rows,int columns, int slices): rows(rows), columns(columns), slices(slices){
        plane_size = slices*rows;
    }
    inline node_id sub2ind(int r, int c, int s) {
        if (!(r >= 0 && c >=0 && s >= 0 )) {
            return -1;
        }
        if (!(r < rows && c < columns && s < slices)) {
            return -1;
        }
        return c*plane_size + rows*s + r;
    }

    inline void getRow(node_id index, int &r) {
        r = (index/plane_size)%rows;
    }

    inline void ind2sub(node_id index, int &r, int &c, int &s) {
        c = index/plane_size;
        node_id rem = index%plane_size;
        s = rem/rows;
        r = rem %rows;
    }

    // No bounds checking performed here!
    inline node_id getNodeAtDirection(node_id id, dir_type direction, distance_type distance) {
        int r,c,s;
        this->ind2sub(id, r,c,s);
        switch (direction) {
        case DIR_BACK:
            return sub2ind(r+distance,c,s-1);
        case DIR_FRONT:
            return sub2ind(r+distance,c,s+1);
        case DIR_LEFT:
            return sub2ind(r+distance,c-1,s);
        case DIR_RIGHT:
            return sub2ind(r+distance,c+1,s);
        case DIR_UP:
            return sub2ind(r-1,c, s);
        case DIR_DOWN:
            return sub2ind(r+1,c,s);
        case DIR_TERM_FROM:
        case DIR_TERM_TO:
        default:
            assert(false);
            //exit (EXIT_FAILURE);
        }
        return 0;
    }
};

class Node { // todo hide details
    int pid;
public:
    size_t r,s;
    bool st:1;
    bool source_connected:1;
    // LockFree access
    bool sink:1;
    bool source:1;

    ranktype rank;
    // Locked Access
    edge_id current_edge;
    edge_id last_edge;
    edge_id relabel_scan_list;

    node_id label;
    capacity_type excess; // denotes the difference between the incoming flow and the outgoing flow at this vertex

    boost::atomic<bool> inQueue;
    boost::atomic<bool> inGlobalQueue;

    // shared vertices are the vertices which are between 2 segments.
    // lockable vertices are the vertices that have a shared vertex in its immediate neighborhood.
    // If a node is lockable, any update to the locked access member variables need to be done with lock.
    bool lockable:1;
    bool shared:1;
    size_t wavenumber;
    node_id id;
public:
    SpinLock lock; // TODO refactor locking mechanism to a inline function
    Node();
    Node *setLockable(bool val);
    void setShared(bool val);

    inline void setPID(int pid) {
        this->pid = pid;
    }

    inline int getPID() {
        return this->pid;
    }

    bool isSink();
    bool isSource();
    bool isLockable();
    inline bool isConnectedToSource(){
        return this->source_connected;
    }
    inline bool getSTPartition() {
        return this->st;
    }

    bool isShared();
    void setSink();
    void setSource();

};

class ThreadStructures {
    boost::mutex process_queue_mutex; // 40
    boost::mutex relabel_queue_mutex; // 40
    boost::atomic<size_t> queue_counter;// 8
    boost::atomic<bool> relabel_queue_empty[2];
    bool m_queue_sizeset;
    bool process_queue_empty;
    size_t size;
    //boost::mutex process_queue_empty_mutex;
    boost::circular_buffer<Node*>  *process_queue;
    boost::circular_buffer<Node*>  *relabel_queue[2];

public:
    static boost::shared_mutex process_queue_empty_mutex;
    ThreadStructures() :
        queue_counter(0), m_queue_sizeset(false) {
        relabel_queue_empty[0] = true;
        relabel_queue_empty[1] = true;
        sink.setSink();
        source.setSource();
        sink.setLockable(true);
        source.setLockable(true);
        //p_front = p_back = r_front[0] = r_front[1] = r_back[0] = r_back[1] = 0;
    }

    proc_type pid;
    boost::atomic<bool> m_brelabel; // 1
    //char _padding_[32];
    Node sink, source;
    node_id segment_start;
    node_id segment_end;
    size_t internal_capacity;


    void setQueueCapacity(size_t size) {
        this->size = size;
        m_queue_sizeset= true;
        process_queue = new boost::circular_buffer<Node*>(size);
        relabel_queue[0] = new boost::circular_buffer<Node*>(size);
        relabel_queue[1] = new boost::circular_buffer<Node*>(size);
    }

    // lock node if lockable before calling
    inline void addToProcessQueue(Node *node) {
        boost::shared_lock<boost::shared_mutex> _global_lock(ThreadStructures::process_queue_empty_mutex);
        assert(node->getPID() == pid);
        boost::mutex::scoped_lock _local_lock(this->process_queue_mutex);
        if (!node->inQueue) {
            node->inQueue = true;
            queue_counter++;
            process_queue->push_back(node);
        }
    }

    inline bool isProcessQueueEmpty() {
        boost::mutex::scoped_lock _local_lock(this->process_queue_mutex);
        return queue_counter == 0;
    }

    // called only by this process.
    // returns locked vertex
    inline bool getFromProcessQueue(Node *&v) {
        boost::shared_lock<boost::shared_mutex> _global_lock(ThreadStructures::process_queue_empty_mutex);
        boost::mutex::scoped_lock _local_lock(this->process_queue_mutex);

        if (process_queue->empty()) {
            v = NULL;
            return false;
        }

        v = process_queue->front();
        process_queue->pop_front();
        if (v == NULL) {
            return false;
        }
        if (v->isLockable()) {
            v->lock.lock();
        }

        v->inQueue = false;
        return true;
    }

    inline void decrementProcessQueueCounter() {
        boost::shared_lock<boost::shared_mutex> _global_lock(ThreadStructures::process_queue_empty_mutex);
        boost::mutex::scoped_lock _local_lock(this->process_queue_mutex);
        --queue_counter;
    }

    // lock the node before calling!
    inline void addToRelabelQueue(Node *node, int queuenumber) {
        assert(node->getPID() == pid);
        boost::mutex::scoped_lock _local_lock(this->relabel_queue_mutex);

        this->relabel_queue_empty[queuenumber] = false;
        if (!node->inGlobalQueue) {
            node->inGlobalQueue = true;
            relabel_queue[queuenumber]->push_back(node);
        }
    }

    inline bool getFromGlobalQueue(Node *&v, int queuenumber) {

        boost::mutex::scoped_lock _local_lock(this->relabel_queue_mutex);


        if (relabel_queue[queuenumber]->empty()){
            v = NULL;
            this->relabel_queue_empty[queuenumber] = true;
            return false;
        }

        v = relabel_queue[queuenumber]->front();
        relabel_queue[queuenumber]->pop_front();

        if (v == NULL) {
            this->relabel_queue_empty[queuenumber] = true;
            return false;
        }

        if (v->isLockable()){
            v->lock.lock();
        }

        v->inGlobalQueue = false;
        return true;
    }

    inline bool isRelabelQueueEmpty(int queuenumber) {
        boost::mutex::scoped_lock _local_lock(this->relabel_queue_mutex);
        return this->relabel_queue_empty[queuenumber];
    }
};


class Graph {
    ofstream f;
    bool stop_processes;
    Node* nodes;
    edge_id num_edges;
    node_id num_nodes;
    proc_type num_procs;
    size_t rows, columns, slices;
    size_t cols_per_segment;
    size_t m_wavenumber;
    node_id m_currentLevel;
    // Edges
    capacity_type *rcap;
    capacity_type *histogram;
    edge_offset_type *edge_mate_offset;


    SizingOracle so;

    boost::atomic<size_t> m_dischargecounter;
    float gr_factor;

    boost::condition_variable m_lsgr_condition;
    boost::mutex m_lsgr_mutex;

    boost::atomic<bool> *m_done_flags;

    ThreadStructures *m_tsdata;

    void mark_shared(size_t i);
    void mark_lockable(size_t i);


    void initialize_preflow();

    void discharge(proc_type pid);
    status apply(Node *u, proc_type pid);
    void relabel(Node *v, proc_type pid);
    void addToQueue(Node *v);
    void search_allocator();
    void global_relabel(proc_type pid);

    bool leveldone();


    void updateLabelCounter(size_t oldValue, size_t newValue);

    bool getFromQueue(Node *&v, int pid);
    void queueDecrementCounter(int pid);
    bool isQueueEmpty();
    void addToGlobalQueue(Node *v, int pid, int evenodd);
    bool getFromGlobalQueue(Node *&v, int pid, int evenodd);
    bool isGlobalQueueEmpty(int evenodd);

    inline capacity_type getEdgeCap(edge_id id) {
        return rcap[id];
    }
    inline edge_offset_type getEdgeOffset(edge_id e, offset_type offset) {
        int r,c,s;
        so.ind2sub(e/EDGES_PER_NODE, r,c,s);
        return edge_mate_offset[r*EDGES_PER_NODE+offset];
    }

    inline edge_id getEdgeMate(edge_id eid, Node *node) {

        offset_type offset = eid % EDGES_PER_NODE;
        if (offset == DIR_TERM_FROM) {
            return eid - offset + DIR_TERM_TO;
        }
        if (offset == DIR_TERM_TO) {
            return eid - offset + DIR_TERM_FROM;
        }

        size_t eo = (node->s*(rows*EDGES_PER_NODE))+(node->r*EDGES_PER_NODE)+offset;
        if (edge_mate_offset[eo] != 0) {
            edge_id e = (node->id*EDGES_PER_NODE + edge_mate_offset[eo]);
            return (e < 0 || e >= num_nodes*EDGES_PER_NODE) ? -1 : e;
        }


        dir_type direction;
        distance_type distance;
        decodeOffset(offset,direction,distance);

        node_id mate_node = so.getNodeAtDirection(node->id, direction, distance);
        if (mate_node < 0 || mate_node >= num_nodes) {
            return -1;
        }

        dir_type mate_dir = getMateDirection(direction);

        edge_id mateid = mate_node *EDGES_PER_NODE + mate_dir;
        if (mate_dir > DIR_DOWN) {
             mateid += ARC_LENGTH + distance;
        }
        assert(mateid >=0 && mateid < num_nodes*EDGES_PER_NODE);
        if (edge_mate_offset[eo] != 0) {
            assert(edge_mate_offset[eo] == mateid - node->id*EDGES_PER_NODE);
        } else {
            edge_mate_offset[eo] = mateid - node->id*EDGES_PER_NODE;
        }
        return mateid;
    }


    inline Node* getNodeMate(edge_id eid, proc_type pid, Node *node) {
        offset_type offset = eid % EDGES_PER_NODE;
        if(offset == DIR_TERM_TO) {
            return node->isConnectedToSource() ? &this->m_tsdata[pid].source : &this->m_tsdata[pid].sink;
        } else if (offset == DIR_TERM_FROM) {
            return &nodes[eid/EDGES_PER_NODE];
        }
        edge_id mate_id = getEdgeMate(eid, node);
        if (mate_id < 0) {
            return NULL;
        }
        return &nodes[mate_id/EDGES_PER_NODE];
    }


    inline void increaseFlowAlongEdge(capacity_type del, edge_id eid, Node* node) {
        edge_id mateid = getEdgeMate(eid, node);
        assert(mateid >= 0);
        rcap[eid] -= del;
        rcap[mateid] += del;
    }

    // negative distances go up, and positive distances go down
    inline void decodeOffset(offset_type o, dir_type &dir, distance_type &dis) {
        dis = 0;
        switch(o) {
        case DIR_TERM_FROM:
            dir = DIR_TERM_FROM;
            return;
        case DIR_TERM_TO:
            dir = DIR_TERM_TO;
            return;
        case DIR_UP:
            dir = DIR_UP;
            return;
        case DIR_DOWN:
            dir = DIR_DOWN;
            return;
        default:
            o -= DIR_LEFT;
            dir = o/(EDGES_PER_DIR);
            dis = o%EDGES_PER_DIR;
            dir = dir *EDGES_PER_DIR;
            dir += DIR_LEFT;
            dis = ARC_LENGTH-dis;
            return;
        }
    }

    // Given any direction, this function returns the direction of this
    // node to its mate
    inline dir_type getMateDirection(dir_type dir) {
        assert(dir >=0 && dir < EDGES_PER_NODE);
        if (dir < DIR_LEFT) {
            return (dir%2==0) ? dir+1 : dir -1;
        }
        else {
            return (dir%2 == 0) ? dir + 21 : dir - 21;
        }
    }

    // This method populates the edge mate offsets for all offsets
    void partition_nodes();

public:
    Graph (size_t num_edges, size_t num_nodes, size_t rows, size_t cols, size_t slices, size_t num_procs = 0, float gr_factor = 1);
    ~Graph() {
        delete [] nodes;
        delete [] rcap;
        delete [] m_done_flags;
        delete [] m_tsdata;
    }

    inline size_t getRows() {
        return rows;
    }
    inline size_t getCols() {
        return columns;
    }
    inline size_t getSlices() {
        return slices;
    }
    inline int what_segment(int r,int c, int s) {
        return nodes[so.sub2ind(r,c,s)].getSTPartition() ? 1: 0;
    }

    void add_edge(unsigned int r, unsigned int c, unsigned int s, dir_type dir, distance_type offset, int cap);
    capacity_type maxflow();
    capacity_type  getFlowValue();

};

#endif // DS_H
