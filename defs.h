#ifndef DEFS_H
#define DEFS_H

#include <sys/time.h>
#include <assert.h>


#define DEF_NPROC 40
#define G_COUNTER_UPDATE_FREQ 1000
#define GR_SLEEP_TIME 1
#define DO_GLOBAL_RELABEL true
//#define SEGMENT_BY_TWEIGHTS

typedef int capacity_type;
typedef  short int offset_type;
typedef long int node_id;
typedef long int edge_id;
typedef char dir_type;
typedef char distance_type;
typedef unsigned char proc_type;
typedef  int edge_offset_type;

#define EDGES_PER_DIR 21
#define EDGES_PER_NODE 88
#define ARC_LENGTH 10 // different name to avoid conflicts with arc_window
// todo remove and use arc_window

#define DIR_TERM_FROM   0
#define DIR_TERM_TO 1
#define DIR_UP     2
#define DIR_DOWN   3
#define DIR_LEFT   4
#define DIR_RIGHT  25
#define DIR_FRONT  46
#define DIR_BACK   67

#define EDGE_START(i) (i*EDGES_PER_NODE)
#endif // DEFS_H




