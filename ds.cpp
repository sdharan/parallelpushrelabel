#include "ds.h"
#include <unistd.h>

boost::shared_mutex ThreadStructures::process_queue_empty_mutex;
Node::Node(): sink(false), source(false), lockable(false), shared(false), wavenumber(0)/*,q_next(NULL), q_prev(NULL),gq_next(NULL), gq_prev(NULL)*/ {
    label = 0;
    st = true;
    excess = 0;
    inQueue = false;
    inGlobalQueue = false;
    source_connected = false;
    rank = WHITE;
}

bool Node::isSink() {
    return sink;
}

bool Node::isSource() {
    return source;
}

Node *Node::setLockable(bool val) {
    this->lockable = val;
    return this;
}

void Node::setShared(bool val) {
    this->shared = val;
}

bool Node::isLockable() {
    return lockable;
}

bool Node::isShared() {
    return shared;
}

void Node::setSink()
{
    assert(!this->source);
    this->sink = true;
}

void Node::setSource()
{
    assert(!this->sink);
    this->source = true;
}

void Graph::relabel(Node *v, proc_type pid) {
    edge_id pos_current_edge = EDGE_START(v->id);
    node_id poslabel = num_nodes;

    if (v->excess == 0) {
        return ;
    }

    bool set = false;
    edge_id edge = EDGE_START(v->id);
    int counter  = 0;
    while (edge< v->last_edge) {
        if (getEdgeCap(edge) > 0) {
            Node *u = getNodeMate(edge,pid,v);
            if ((u != NULL) && (poslabel > u->label)) {
                poslabel =   u->label;
                pos_current_edge = edge;
                set = true;
            }
        }
        if (slices == 1 && counter > DIR_FRONT) {
            break;
        }
        edge++;
        counter++;
    }

    if (!set) {
        v->label = num_nodes + 1;
    } else if (poslabel >= v->label) {
        v->label = poslabel + 1;
    }
    if (set && v->label < num_nodes) {
        v->current_edge = pos_current_edge;
    } else {
        v->current_edge = EDGE_START(v->id);
    }

}

void Graph::addToQueue(Node *v)
{
    if (!v->inQueue) {
        m_tsdata[v->getPID()].addToProcessQueue(v);
    }
}

status Graph::apply(Node *u, proc_type pid) {

    if (u->label >= num_nodes || u->excess == 0) {
        if (u->isLockable()){
            u->lock.unlock();
        }
        return END;
    }

    assert(u->label <=  num_nodes);

    edge_id edge = u->current_edge;

    while((edge < u->last_edge) && (u->excess > 0)) {
        if (edge-EDGE_START(u->id) == DIR_TERM_FROM) {
            edge = ++u->current_edge;
            continue;
        }

        if (slices == 1 && edge%EDGES_PER_NODE >= DIR_FRONT){
            edge = u->current_edge = u->last_edge;
            break;
        }

        if(getEdgeCap(edge) > 0) {
            Node *v = getNodeMate(edge,pid, u);
            if (v == NULL){ // edge does not exist
                u->current_edge++;
                edge++;
                continue;
            }

            if (v->isLockable())  {
                // Get lock
                if (!v->lock.try_lock()){
                    // Release all locks and return as WAIT
                    if (u->excess > 0 && u->label < num_nodes) {
                        addToQueue(u);
                    }

                    return WAIT;
                }
            }

            // Locked all the required objects
            if (v->label == u->label - 1 && (v->isSink() || v->wavenumber == u->wavenumber)) {
                capacity_type del = std::min(u->excess, getEdgeCap(edge));
                assert(del > 0);
                increaseFlowAlongEdge(del,edge, u);
                u->excess = u->excess - del;
                v->excess = v->excess + del;


                // If v has become active, then add it to the queue;
                if (v->excess > 0 && v->label < num_nodes  && !v->source && !v->sink){
                    addToQueue(v); // adds to queue if its not already in the queue.
                }
            }

            if (v->isLockable()) {
                v->lock.unlock();
            }
        }
        edge = ++u->current_edge;

    }
    relabel(u,pid);
    if ( u->label < num_nodes && u->excess > 0) {
        addToQueue(u);
    }

    return END;
}

capacity_type Graph::getFlowValue() {
    size_t flow = 0;
    for (size_t i = 0; i < num_procs; ++i) {
        flow += this->m_tsdata[i].sink.excess;
    }
    return flow;
}


Graph::Graph(size_t num_edges, size_t num_nodes, size_t rows, size_t cols, size_t slices, size_t num_procs, float gr_factor)
    : stop_processes(false),
      num_edges(num_edges),
      num_nodes(num_nodes),
      num_procs(num_procs),
      rows(rows),
      columns(cols),
      slices(slices),
      m_wavenumber(0),
      m_currentLevel(0),
      m_dischargecounter(0),
      gr_factor(gr_factor)
{
    stop_processes = false;
    so = SizingOracle(rows, columns, slices);
    rcap = new capacity_type[num_nodes*EDGES_PER_NODE]();
    histogram = new capacity_type[columns]();
    m_tsdata = new ThreadStructures[num_procs]();
    edge_mate_offset = new edge_offset_type[EDGES_PER_NODE * rows * slices];
    memset(edge_mate_offset, 0, sizeof(edge_offset_type)*EDGES_PER_NODE * rows * slices);

    for (size_t i = 0; i < num_procs; ++i) {
        m_tsdata[i].source.label = num_nodes+1;
    }

    nodes = new Node[num_nodes];

    cols_per_segment = columns/num_procs;

    for (size_t k = 0; k < slices; k++) {
        for (size_t i = 0; i < rows; ++i) {
            for (size_t j = 0; j < cols; j++) {
                size_t index = so.sub2ind(i,j,k);
                assert(index >= 0);
                nodes[index].r =  i;
                nodes[index].s = k;
                nodes[index].id = index;
                nodes[index].current_edge = EDGE_START(index);
                nodes[index].last_edge = nodes[index].current_edge + EDGES_PER_NODE;
            }
        }
    }


    m_done_flags = new boost::atomic<bool>[num_procs]();

    for (size_t i = 0; i < num_procs; ++i) {
        m_done_flags[i] = false;
    }
    // Queue implementation.

}

capacity_type Graph::maxflow() {
    std::cout << "Starting Maxflow on graph with " << num_nodes << " nodes " << (int)num_procs << " segments and gr_factor = " << gr_factor << std::endl;

    time_t start;
    time(&start);
    this->initialize_preflow();
    std::cout << "Finished Initializing Preflow. Creating Threads ..." << std::endl;

    // create threads.
    boost::thread_group tg;
    for (proc_type i = 0; i < num_procs; ++i) {
        tg.create_thread(boost::bind(&Graph::discharge, this, i));
    }
    tg.create_thread(boost::bind(&Graph::search_allocator, this));

    tg.join_all();
    time_t end;
    time(&end);
    std::cout << "timetaken =  " << difftime(start,end) << std::endl << std::flush;


    // TODO
    // Recode the following code to run in parallel. Contributes to about 5% of the actual running time :/

    std::cout << "Completing Segmentation ..." << std::endl;

    std::queue <Node*> localq;

    for (node_id nid = 0; nid < num_nodes; ++nid) {
        if (!nodes[nid].source_connected && getEdgeCap(EDGE_START(nid)+1) > 0) {
            localq.push(&nodes[nid]);
            nodes[nid].rank = GREY;
            nodes[nid].st = false;
        }
    }

    while (!localq.empty()) {
        Node *n = localq.front();
        localq.pop();
        edge_id e = EDGE_START(n->id) + 1;

        while (++e < n->last_edge) {
            if (slices == 1 && (e)%EDGES_PER_NODE >= DIR_FRONT) {
                break;
            }
            Node *nu = getNodeMate(e,0,n);
            if (nu == NULL) {
                continue;
            }
            if (getEdgeCap(getEdgeMate(e,n)) > 0 && nu->rank == WHITE) {
                assert(!nu->isSink() && !nu->isSource());
                nu->rank = GREY;
                localq.push(nu);
                nu->st = false;
            }
        }
    }

    return this->getFlowValue();
}

void Graph::global_relabel(proc_type pid) {


    if (m_currentLevel == num_nodes+1) {
        for (node_id nid = m_tsdata[pid].segment_start; nid < m_tsdata[pid].segment_end; ++nid) {
            nodes[nid].lock.lock();
            if (nodes[nid].wavenumber < m_wavenumber) {
                nodes[nid].label = num_nodes+2;
            }
            nodes[nid].lock.unlock();
        }
        return;
    }

    if (m_currentLevel == 1) {
        // The localq queue will be empty, just get the lock on sink and add all the edges
        // leading to nodes in this segment

        Node *s = &this->m_tsdata[pid].sink;
        s->lock.lock(); // This lock is redundant. remove after testing.

        node_id nid = m_tsdata[pid].segment_start;
        for (edge_id e = nid*EDGES_PER_NODE + DIR_TERM_TO; nid < m_tsdata[pid].segment_end; e += EDGES_PER_NODE, ++nid) {

            if (nodes[nid].source_connected) {
                continue;
            }

            if (getEdgeCap(e) > 0) { // This is an edge going to the sink, no other processor would update it. no locking needed
                Node *u = &nodes[nid];
                if (u->isLockable()) {
                    u->lock.lock();
                }
                // Get a lock on this node. There cannot be a deadlock here,
                // because u has to be processed by the same processor.
                if (u->wavenumber == m_wavenumber){
                    if (u->isLockable()) {
                        u->lock.unlock();
                    }
                    continue;
                }
                assert(u->wavenumber < m_wavenumber);

                assert(u->getPID() == pid);
                u->wavenumber = m_wavenumber;
                if (u->label < m_currentLevel) {
                    u->label = m_currentLevel;
                }

                this->addToGlobalQueue(u,pid, (m_currentLevel+1)%2);

                u->relabel_scan_list = EDGE_START(nid);

                // if this node  becomes active, add it to the queue.
                if (!u->inQueue && u->excess > 0 && u->label < num_nodes){
                    std::cerr << "Active vertex not in queue!" << std::endl;
                    assert(false); // should never come here
                }

                if (u->isLockable()) {
                    u->lock.unlock();
                }
            }
        }

        s->lock.unlock();
        return;
    }

    // Get values from the queue and process them.
    Node *v;
    // v gets locked in getFromGlobalQueue
    while (getFromGlobalQueue(v, pid,m_currentLevel%2)) {
        assert (v->wavenumber == m_wavenumber);

        edge_id e = v->relabel_scan_list;

        while (e < v->last_edge) {
            if (slices == 1 && e%EDGES_PER_NODE >=DIR_FRONT) {
                break;
            }
            Node *u = getNodeMate(e,pid, v);
            if (u == NULL || u == v){
                ++e;
                continue;
            }

            if (!(u->isSink() || u->isSource())) {

                if (u->isLockable()) {
                    if (!u->lock.try_lock()) {
                        this->addToGlobalQueue(v, pid, m_currentLevel%2);

                        break;
                    }
                }
                // all elements are locked here.

                edge_id mateedge = getEdgeMate(e, v);

                if ((mateedge >=0) && (m_currentLevel > num_nodes || getEdgeCap(mateedge) > 0) && u->wavenumber < m_wavenumber) {
                    this->addToGlobalQueue(u,u->getPID(), (m_currentLevel+1)%2);

                    u->relabel_scan_list = EDGE_START(u->id);
                    u->wavenumber = m_wavenumber;
                    if (u->label < m_currentLevel) {
                        u->label= m_currentLevel;
                    }
                }

                if (u->excess > 0 && u->label < num_nodes){
                    addToQueue(u);
                }

                if (u->isLockable()) {
                    u->lock.unlock();
                }

            }
            ++e;
        }

        if (v->isLockable()) {
            v->lock.unlock();
        }
    }
}

bool Graph::getFromQueue(Node *&v, int pid) {
    return this->m_tsdata[pid].getFromProcessQueue(v);
}


// If v is to be locked, it has to be locked by the calling function
void Graph::addToGlobalQueue(Node *v, int pid, int evenodd) {

    if (!v->inGlobalQueue) {
        return this->m_tsdata[v->getPID()].addToRelabelQueue(v, evenodd);
    }
}

bool Graph::isGlobalQueueEmpty(int evenodd){
    assert(evenodd == 0 || evenodd == 1);
    for (size_t i = 0; i < num_procs; ++i) {
        if (!m_tsdata[i].isRelabelQueueEmpty(evenodd)) {
            return false;
        }
    }

    return true;
}

bool Graph::getFromGlobalQueue(Node *&v, int pid, int evenodd) {
    return m_tsdata[pid].getFromGlobalQueue(v, evenodd);
}



void Graph::queueDecrementCounter(int pid) {
    m_tsdata[pid].decrementProcessQueueCounter();

}

bool Graph::isQueueEmpty() {
    boost::unique_lock< boost::shared_mutex > lock(ThreadStructures::process_queue_empty_mutex);
    for (proc_type p = 0; p < num_procs; ++p) {
        if (!m_tsdata[p].isProcessQueueEmpty()) {
            return false;
        }
    }

    stop_processes = true;
    return true;
}

// Main discharge loop
void Graph::discharge(proc_type pid) {
    int localcounter = 0;
    size_t waitcounter = 0;
    int sleepcounter = 0;
    bool check_queue = false;
    while (true) {
        Node *v = NULL;
        while (getFromQueue(v,pid)) {
            check_queue = true;
            if (apply(v,pid) == WAIT) {
                waitcounter++;
            } else {
                localcounter++;
            }
            if (v->isLockable()) {
                v->lock.unlock();
            }
            // update only after certain number of operations.
            if (localcounter == G_COUNTER_UPDATE_FREQ) {
                m_dischargecounter += G_COUNTER_UPDATE_FREQ;
                localcounter = 0;
            }

            if (localcounter == 0 && (this->m_tsdata[pid].m_brelabel == DO_GLOBAL_RELABEL)) {
                // Start relabelling.
                this->global_relabel(pid);
                this->m_lsgr_mutex.lock();
                this->m_tsdata[pid].m_brelabel = !DO_GLOBAL_RELABEL;
                this->m_lsgr_mutex.unlock();
                m_lsgr_condition.notify_one();
            }
            this->queueDecrementCounter(pid);
        }

        if (!stop_processes) {

            if (check_queue){
                if (this->isQueueEmpty()) {
                    break;
                }
                check_queue = false;
            }

            boost::this_thread::yield();
            if (localcounter > 0) {
                m_dischargecounter += localcounter;
                localcounter  = 0;
            }
            sleepcounter++;
            if ((this->m_tsdata[pid].m_brelabel == DO_GLOBAL_RELABEL)) {
                // Start relabelling.
                this->global_relabel(pid);
                this->m_lsgr_mutex.lock();
                this->m_tsdata[pid].m_brelabel = !DO_GLOBAL_RELABEL;
                this->m_lsgr_mutex.unlock();
                m_lsgr_condition.notify_one();
            }
        } else {
            break;
        }
    }

    {
        boost::mutex::scoped_lock lock(m_lsgr_mutex);
        this->m_tsdata[pid].m_brelabel = !DO_GLOBAL_RELABEL;
        m_lsgr_condition.notify_one();
//        std::cerr << "Wait Counter: " << waitcounter << std::endl;
//        std::cerr << "Sleep Counter: " << sleepcounter << std::endl;

    }

}

// Global relabel allocator.
void Graph::search_allocator() {
    m_wavenumber = 0;
    m_currentLevel = 0;
    while(true) {
        if (m_dischargecounter< gr_factor*num_nodes) {
            sleep(GR_SLEEP_TIME);
            if (stop_processes) {
                break;
            }
        }

        boost::mutex::scoped_lock lock(m_lsgr_mutex);

        m_wavenumber++;

        std::cerr << " The max label is " << m_currentLevel << std::endl << std::flush;
        m_currentLevel = 1;

        for (size_t i = 0; i < num_procs; ++i){
            this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
        }

        while (!leveldone()) {
            m_lsgr_condition.wait(lock);
            if (stop_processes) {
                return;
            }
        }

        while (!isGlobalQueueEmpty((m_currentLevel+1)%2) && !stop_processes) {
            m_currentLevel++;
            for (size_t i = 0; i < num_procs; ++i){
                this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
            }

            while (!leveldone()) {
                m_lsgr_condition.wait(lock);
                if (stop_processes) {
                    return;
                }
            }
        }

        m_currentLevel = num_nodes+1;

        for (size_t i = 0; i < num_procs; ++i){
            this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
        }

        while (!leveldone()) {
            m_lsgr_condition.wait(lock);
            if (stop_processes) {
                return;
            }
        }

        m_dischargecounter = 0;
    }
}

bool Graph::leveldone() {
    for(size_t i = 0; i < num_procs; ++i) {
        if (this->m_tsdata[i].m_brelabel == DO_GLOBAL_RELABEL) {
            return false;
        }
    }
    return true;
}

void Graph::mark_shared(size_t i){
    assert(i < num_nodes && "index out of bounds in Graph::mark_shared");
    nodes[i].setShared(true);
}

void Graph::mark_lockable(size_t i) {
    assert(i < num_nodes && "index out of bounds in Graph::mark_lockable");
    nodes[i].setLockable(true);
}

void Graph::partition_nodes() {
#ifdef SEGMENT_BY_TWEIGHTS
    for (node_id i = 0; i < num_nodes; ++i){
        int r,c,s;
        so.ind2sub(i,r,c,s);
        edge_id source_edge = EDGE_START(i) + DIR_TERM_FROM;
        capacity_type cap = getEdgeCap(source_edge);
        histogram[c] += nodes[i].source_connected ? cap : -1*cap;
    }

   capacity_type total_cap = 0;
   for (size_t i = 0; i < columns; ++i) {
       total_cap += histogram[i];
   }
   capacity_type optimal_segment_cap = total_cap/num_procs;
   proc_type cur_seg = 0;
   capacity_type segment_cap = 0;
   size_t segment_ends[num_procs];
   memset (segment_ends,0, sizeof(size_t)*num_procs);
   bool mark_shared = false;
#endif

   for (size_t j = 0; j < columns; j++) {
#ifdef SEGMENT_BY_TWEIGHTS
       if (segment_cap > optimal_segment_cap && cur_seg < num_procs - 1) {
           segment_ends[cur_seg] = j-1;
           cur_seg++;
           segment_cap -= optimal_segment_cap;
           mark_shared = true;
       }
       else {
           mark_shared = false;
       }
       segment_cap += histogram[j];
#endif
       for (size_t k = 0; k < slices; k++) {
           for (size_t i = 0; i < rows; ++i) {
                size_t index = so.sub2ind(i,j,k);
                assert(index >= 0);
#ifdef SEGMENT_BY_TWEIGHTS
                nodes[index].setPID(cur_seg);
                if (mark_shared) {
                    nodes[index].setLockable(true);
                    nodes[index].setShared(true);
                    nodes[so.sub2ind(i,j-1,k)].setLockable(true);
                    nodes[so.sub2ind(i,j+1,k)].setLockable(true);
                }
#else
                nodes[index].setPID(std::min((int)(j / cols_per_segment), num_procs-1));
                if (j > 0 && j < columns - 1 && j%(columns/num_procs) == 0) {
                    nodes[index].setLockable(true);
                    nodes[index].setShared(true);
                    nodes[so.sub2ind(i,j-1,k)].setLockable(true);
                    nodes[so.sub2ind(i,j+1,k)].setLockable(true);
                }

#endif
            }
        }
    }

#ifdef SEGMENT_BY_TWEIGHTS
   for(proc_type p = 0; p < num_procs; p++){
       if (p == 0) {
           m_tsdata[p].segment_start = so.sub2ind(0,0,0);
       } else {
           m_tsdata[p].segment_start = so.sub2ind(0, segment_ends[p-1]+1, 0 );
       }
       if (p < num_procs-1) {
           m_tsdata[p].segment_end  = so.sub2ind(rows-1, segment_ends[p], slices-1 )+1;
       } else {
           m_tsdata[p].segment_end  = so.sub2ind(rows-1, columns-1, slices-1 )+1;
       }
       m_tsdata[p].setQueueCapacity(m_tsdata[p].segment_end - m_tsdata[p].segment_start);
       m_tsdata[p].pid = p;
   }

#else
   for(proc_type p = 0; p < num_procs; p++){
       m_tsdata[p].segment_start = so.sub2ind(0, (cols_per_segment)*p, 0 );
       assert(m_tsdata[p].segment_start >= 0);
       if (p < num_procs-1) {
           m_tsdata[p].segment_end  = so.sub2ind(rows-1, ((cols_per_segment)*(p+1))-1, slices-1 )+1;
           assert( m_tsdata[p].segment_end>= 0);
       } else {
           m_tsdata[p].segment_end  = so.sub2ind(rows-1, columns-1, slices-1 )+1;
           assert( m_tsdata[p].segment_end>= 0);

       }
       m_tsdata[p].setQueueCapacity(m_tsdata[p].segment_end - m_tsdata[p].segment_start);
       m_tsdata[p].pid = p;

   }
#endif
}

void Graph::add_edge(unsigned int r, unsigned int c, unsigned int s, dir_type dir, distance_type offset, int cap) {
    node_id index = so.sub2ind(r,c,s);

    assert(index >=0 && index < num_nodes);

    if (dir == DIR_TERM_FROM || dir == DIR_TERM_TO) {
        if (cap < 0) { // connect it to the source
            this->rcap[index*EDGES_PER_NODE + DIR_TERM_FROM] -= cap;
            nodes[index].source_connected = true;
        }
        else {
            this->rcap[index*EDGES_PER_NODE + DIR_TERM_TO] += cap;
            nodes[index].source_connected = false;
        }
        return;
    }

    assert (cap > 0);
    assert (offset < ARC_LENGTH);

    edge_id e = EDGE_START(index) + dir;
    if (dir > DIR_DOWN) {
        e += ARC_LENGTH - offset;
    }
    this->rcap[e] += cap;

    return;
}


void Graph::initialize_preflow() {
    this->partition_nodes();

    for (node_id i = 0; i < num_nodes; ++i) {

        edge_id source_edge = EDGE_START(i) + DIR_TERM_FROM;
        capacity_type cap = getEdgeCap(source_edge);
        if (nodes[i].source_connected && cap > 0) {
            increaseFlowAlongEdge(cap, source_edge, &nodes[i]);

            nodes[i].excess += cap;
            m_tsdata[nodes[i].getPID()].internal_capacity+= cap;
            addToQueue(&nodes[i]);
        }
        nodes[i].current_edge = EDGE_START(i);
    }

}

